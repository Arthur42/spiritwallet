type Identity = {
  locked: string;
  distribution: { [key: string]: number };
  identity: string;
  avatar: string;
  description: string;
  name: string;
  website: string;
  location: string;
  score: number;
  validators: number;
  stake: string;
  topUp: string;
  providers: string[];
  stakePercent: number;
  apr?: number;
  rank: number;
};

export default Identity;

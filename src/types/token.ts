export type Token = {
  identifier: string;
  name: string;
  ticker: string;
  owner: string;
  minted: string;
  burnt: string;
  decimals: number;
  isPaused: boolean;
  assets?: {
    website?: string;
    description?: string;
    social?: {
      email?: string;
      blog?: string;
      twitter?: string;
      whitepaper?: string;
    };
    status?: string;
    pngUrl?: string;
    svgUrl?: string;
  };
  accounts: number;
  canUpgrade: boolean;
  canMint: boolean;
  canBurn: boolean;
  canChangeOwner: boolean;
  canPause: boolean;
  canFreeze: boolean;
  canWipe: boolean;
  balance: string;
  supply: string;
  circulatingSupply: string;
};

export default Token;

export type TokensItem = {
  identifier: string;
  name: string;
  ticker: string;
  owner: string;
  minted: string;
  burnt: string;
  decimals: number;
  isPaused: boolean;
  assets?: {
    website?: string;
    description?: string;
    ledgerSignature?: string;
    social?: {
      [key: string]: string;
    };
    status?: string;
    pngUrl?: string;
    svgUrl?: string;
  };
  transactions: number;
  accounts: number;
  canUpgrade: boolean;
  canMint: boolean;
  canBurn: boolean;
  canChangeOwner: boolean;
  canPause: boolean;
  canFreeze: boolean;
  canWipe: boolean;
  supply?: string;
  circulatingSupply?: string;
};

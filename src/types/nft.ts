export type Nft = {
  identifier: string;
  owner?: string;
  collection: string;
  attributes: string;
  nonce: 1;
  type: NftType;
  name: string;
  creator: string;
  royalties?: number;
  uris: string[];
  url: string;
  media: NftMedia[];
  isWhitelistedStorage: boolean;
  tags: string[];
  metadata: {};
  balance: string;
  ticker: string;
  // MetaESDT
  decimals?: number;
  price?: number;
  valueUsd?: number;
};

export type MetaESDT = {
  type: 'MetaESDT';
  assets: unknown;
  attributes: string;
  collection: string;
  creator: string;
  identifier: string;
  isWhitelistedStorage: boolean;
  name: string;
  nonce: number;
  supply: string;
  ticker: string;
  timestamp: number;
  unlockSchedule?: UnlockSchedule[];
} & Nft;

export type UnlockSchedule = {
  percent: number;
  remainingEpochs: number;
};

export default Nft;

export type NftsItem = {
  identifier: string;
  collection: string;
  timestamp: number;
  attributes: string;
  nonce: number;
  type: string;
  name: string;
  creator: string;
  isWhitelistedStorage: boolean;
  decimals: number;
  assets: {
    website: string;
    description: string;
    status: string;
    pngUrl: string;
    svgUrl: string;
  };
  ticker: string;
};

export type NftMedia = {
  url: string;
  fileSize: number;
  fileType: string;
  originalUrl: string;
  thumbnailUrl: string;
};

export type NftType = 'NonFungibleESDT' | 'SemiFungibleESDT' | 'MetaESDT';

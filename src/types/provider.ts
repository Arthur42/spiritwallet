type Provider = {
  provider: string;
  serviceFee: number;
  delegationCap: string;
  apr: number;
  numUsers: number;
  cumulatedRewards: string;
  identity: string;
  numNodes: number;
  stake: string;
  topUp: string;
  locked: string;
  featured: boolean;
};

export default Provider;

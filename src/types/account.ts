type Account = {
  address: string;
  nonce: number;
  balance: string;
  rootHash: string;
  txCount: number;
  scrCount: number;
  username: string;
  shard: number;
  developerReward: string;
};

export default Account;

export type AccountsItem = {
  address: string;
  balance: string;
  shard: number;
  nonce?: number;
};

export type Transaction = {
  txHash: string;
  gasLimit: number;
  gasPrice: number;
  gasUsed: number;
  miniBlockHash: string;
  nonce: number;
  receiver: string;
  receiverShard: number;
  round: number;
  sender: string;
  senderShard: number;
  signature: string;
  status: string;
  value: string;
  fee: string;
  timestamp: number;
  data?: string;
  function?: string;
  action?: {
    category: string;
    name: 'transfer' | 'stake' | string;
    description: string;
    arguments: TransactionActionStake | TransactionActionTransfer;
  };
  results?: TransactionResult[];
  operations?: TransactionOperation[];
  price: number;
  logs?: TransactionLogs;
};

export default Transaction;

export type TransactionsItem = {
  txHash: string;
  gasLimit: number;
  gasPrice: number;
  gasUsed: number;
  miniBlockHash: string;
  nonce: number;
  receiver: string;
  receiverShard: number;
  round: number;
  sender: string;
  senderShard: number;
  signature: string;
  status: string;
  value: string;
  fee: string;
  timestamp: number;
  data?: string;
  function?: string;
  action?: {
    category: string;
    name: 'transfer' | 'stake' | string;
    description: string;
    arguments: TransactionActionStake | TransactionActionTransfer;
  };
  scamInfo?: unknown;
  type?: unknown;
  originalTxHash?: unknown;
  pendingResults?: boolean;
};

export type TransactionLogs = {
  address: string;
  events: TransactionLogsEvents[];
};

export type TransactionActionStake = {
  providerName?: string;
  providerAvatar?: string;
};

export type TransactionActionTransfer = {
  transfers?: TransactionActionTransfers[];
  receiver?: string;
};

export type TransactionActionTransfers = {
  type: string;
  name: string;
  ticker: string;
  svgUrl: string;
  collection?: string;
  token?: string;
  decimals: string;
  identifier?: string;
  value: string;
};

export type TransactionResult = {
  hash: string;
  timestamp: string;
  nonce: number;
  gasLimit: number;
  gasPrice: number;
  value: string;
  sender: string;
  receiver: string;
  prevTxHash: string;
  originalTxHash: string;
  callType: string;
  miniBlockHash?: string;
  data?: string;
  logs: {
    address: string;
    events: TransactionLogsEvents[];
  };
};

export type TransactionLogsEvents = {
  address: string;
  identifier: string;
  topics: string[];
  data?: string;
  order: number;
};

export type TransactionOperation = {
  id: string;
  sender: string;
  receiver: string;
  action: TransactionOperationActionTypes;
  type: TransactionOperationTypes;
  esdtType?: TransactionOperationESDTTypes;
  collection?: string;
  identifier?: string;
  name?: string;
  value?: string;
  decimals?: number;
  data?: string;
};

export type TransactionOperationActionTypes =
  | 'none'
  | 'transfer'
  | 'burn'
  | 'addQuantity'
  | 'create'
  | 'localMint'
  | 'localBurn'
  | 'wipe'
  | 'freeze'
  | 'writeLog'
  | 'signalError';
export type TransactionOperationTypes = 'none' | 'nft' | 'esdt' | 'log' | 'error' | 'egld';
export type TransactionOperationESDTTypes =
  | 'FungibleESDT'
  | 'NonFungibleESDT'
  | 'SemiFungibleESDT'
  | 'MetaESDT';

export type SmartContract = {
  address: string;
  nonce: number;
  balance: string;
  code: string;
  codeHash: string;
  rootHash: string;
  txCount: number;
  scrCount: number;
  shard: number;
  developerReward: string;
  ownerAddress: string;
  isUpgradeable: boolean;
  isReadable: boolean;
  isPayable: boolean;
  isPayableBySmartContract: boolean;
  deployedAt: number;
};

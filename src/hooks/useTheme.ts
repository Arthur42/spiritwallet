import { useCallback, useEffect, useState } from 'react';

export type Theme = 'light' | 'dark';

/**
 * useTheme
 * @return {[theme, changeTheme]}
 */
export default function useSelectedTheme(): [Theme | undefined, () => void] {
  const [actualTheme, setActualTheme] = useState<Theme>();

  useEffect(() => {
    const storedTheme = localStorage.getItem('theme');
    if (storedTheme && (storedTheme === 'light' || storedTheme === 'dark')) {
      setActualTheme(storedTheme);
    } else if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
      setActualTheme('dark');
    } else {
      setActualTheme('light');
    }
  }, []);

  const changeTheme = useCallback(() => {
    const newTheme = actualTheme === 'light' ? 'dark' : 'light';
    localStorage.setItem('theme', newTheme);
    setActualTheme(() => newTheme);
  }, [actualTheme]);

  return [actualTheme, changeTheme];
}

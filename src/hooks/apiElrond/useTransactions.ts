import useApi, { useLazyApi } from '../useApi';
import { TransactionsItem } from '../../types/transaction';
import { useEffect, useState } from 'react';
import { DEFAULT_LIST_SIZE } from '../../utils/config';

type UseTransactionsReturn = [
  transactions: TransactionsItem[] | undefined,
  actualPage: number,
  maxPages: number | undefined,
  refresh: (from: number, size?: number) => void,
  isLoading: boolean,
  hasError: boolean
];

/**
 * useTransactions
 * @param {prop} size number of items per page
 * @return {[transactions, actualPage, maxPage, refresh, isLoading, hasError]}
 */
export default function useTransactions(size: number = DEFAULT_LIST_SIZE): UseTransactionsReturn {
  const [actualPage, setTransactionsActualPage] = useState<number>(1);
  const [request, transactions, isLoading, hasError] = useLazyApi<TransactionsItem[]>();
  const [count] = useApi<number>('https://api.elrond.com/transactions/count');
  const maxPage = count && Math.ceil(count / size);

  const refresh = (page: number) => {
    setTransactionsActualPage(page);
    page -= 1;
    const requestSize = page * size > (count ?? 0) ? (count ?? 0) % (page * size) : size;
    request(`https://api.elrond.com/transactions?from=${page * size}&size=${requestSize}`);
  };

  useEffect(() => {
    refresh(1);
  }, []);

  return [transactions, actualPage, maxPage, refresh, isLoading, hasError];
}

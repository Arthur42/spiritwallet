import useApi, { useLazyApi } from '../useApi';
import Nft, { NftType } from '../../types/nft';
import { useEffect, useState } from 'react';
import { DEFAULT_LIST_SIZE } from '../../utils/config';
import _ from 'lodash';

type UseAccountTNftsReturn<T> = [
  nfts: T[] | undefined,
  actualPage: number,
  maxPages: number | undefined,
  refresh: (from: number, size?: number) => void,
  isLoading: boolean,
  hasError: boolean
];

/**
 * useTransactions
 * @param {prop} from
 * @param {prop} identifier
 * @param {prop} types
 * @param {prop} size number of items per page
 * @return {[transactions, actualPage, maxPage, refresh, isLoading, hasError]}
 */
export default function useNfts<T>(
  from: 'accounts' | 'collections',
  identifier: string,
  types?: NftType[] | undefined,
  size: number = DEFAULT_LIST_SIZE
): UseAccountTNftsReturn<T> {
  const type: string = _.reduce(types, (acc, t) => (acc ? acc + ',' + t : t), '');
  const [actualPage, setTransactionsActualPage] = useState<number>(1);
  const [request, nfts, isLoading, hasError] = useLazyApi<T[]>();
  const [count] = useApi<number>(
    from &&
      identifier &&
      `https://api.elrond.com/${from}/${identifier}/nfts/count
${type ? `?type=${type}` : ''}`
  );
  const maxPage = count && Math.ceil(count / size);

  const refresh = (page: number) => {
    setTransactionsActualPage(page);
    page -= 1;
    const requestSize = page * size > (count ?? 0) ? (count ?? 0) % (page * size) : size;
    const link = `https://api.elrond.com/${from}/${identifier}/
nfts?from=${page * size}&size=${requestSize}${type ? `&type=${type}` : ''}`;
    request(link);
  };

  useEffect(() => {
    if (identifier) {
      refresh(1);
    }
  }, [identifier]);

  return [nfts, actualPage, maxPage, refresh, isLoading, hasError];
}

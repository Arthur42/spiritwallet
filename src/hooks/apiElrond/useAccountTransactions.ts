import useApi, { useLazyApi } from '../useApi';
import Transaction from '../../types/transaction';
import { useEffect, useState } from 'react';
import { DEFAULT_LIST_SIZE } from '../../utils/config';

type UseAccountTransactionsReturn = [
  transactions: Transaction[] | undefined,
  actualPage: number,
  maxPages: number | undefined,
  refresh: (from: number, size?: number) => void,
  isLoading: boolean,
  hasError: boolean
];

/**
 * useTransactions
 * @param {prop} walletAddress
 * @param {prop} size number of items per page
 * @return {[transactions, actualPage, maxPage, refresh, isLoading, hasError]}
 */
export default function useAccountTransactions(
  walletAddress: string,
  size: number = DEFAULT_LIST_SIZE
): UseAccountTransactionsReturn {
  const [actualPage, setTransactionsActualPage] = useState<number>(1);
  const [request, transactions, isLoading, hasError] = useLazyApi<Transaction[]>();
  const [count] = useApi<number>(
    `https://api.elrond.com/accounts/${walletAddress}/transactions/count`
  );
  const maxPage = count && Math.ceil(count / size);

  const refresh = (page: number) => {
    setTransactionsActualPage(page);
    page -= 1;
    const requestSize = page * size > (count ?? 0) ? (count ?? 0) % (page * size) : size;
    request(
      `https://api.elrond.com/accounts/${walletAddress}/transactions?from=${
        page * size
      }&size=${requestSize}`
    );
  };

  useEffect(() => {
    if (walletAddress) {
      refresh(1);
    }
  }, [walletAddress]);

  return [transactions, actualPage, maxPage, refresh, isLoading, hasError];
}

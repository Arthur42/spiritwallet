import useApi, { useLazyApi } from '../useApi';
import { AccountsItem } from '../../types/account';
import { useEffect, useState } from 'react';
import { DEFAULT_LIST_SIZE } from '../../utils/config';

type UseAccountsReturn = [
  accounts: AccountsItem[] | undefined,
  actualPage: number,
  maxPages: number | undefined,
  refresh: (from: number, size?: number) => void,
  isLoading: boolean,
  hasError: boolean
];

/**
 * useTransactions
 * @param {prop} size number of items per page
 * @return {[transactions, actualPage, maxPage, refresh, isLoading, hasError]}
 */
export default function useAccounts(size = DEFAULT_LIST_SIZE): UseAccountsReturn {
  const [actualPage, setTransactionsActualPage] = useState<number>(1);
  const [request, accounts, isLoading, hasError] = useLazyApi<AccountsItem[]>();
  const [count] = useApi<number>('https://api.elrond.com/accounts/count');
  const maxPage = count && Math.ceil(count / size);

  const refresh = (page: number) => {
    setTransactionsActualPage(page);
    page -= 1;
    const requestSize = page * size > (count ?? 0) ? (count ?? 0) % (page * size) : size;
    request(`https://api.elrond.com/accounts?from=${page * size}&size=${requestSize}`);
  };

  useEffect(() => {
    refresh(1);
  }, []);

  return [accounts, actualPage, maxPage, refresh, isLoading, hasError];
}

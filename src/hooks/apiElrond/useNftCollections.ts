import useApi, { useLazyApi } from '../useApi';
import { useEffect, useState } from 'react';
import { DEFAULT_LIST_SIZE } from '../../utils/config';
import { NftCollectionsItem } from '../../types/nftCollection';

type UseNftCollectionsReturn = [
  nftCollections: NftCollectionsItem[] | undefined,
  actualPage: number,
  maxPages: number | undefined,
  refresh: (from: number, size?: number) => void,
  isLoading: boolean,
  hasError: boolean
];

/**
 * useTransactions
 * @param {prop} size number of items per page
 * @return {[nftCollections, actualPage, maxPage, refresh, isLoading, hasError]}
 */
export default function useNftCollections(
  size: number = DEFAULT_LIST_SIZE
): UseNftCollectionsReturn {
  const [actualPage, setTransactionsActualPage] = useState<number>(1);
  const [request, nftCollections, isLoading, hasError] = useLazyApi<NftCollectionsItem[]>();
  const [count] = useApi<number>('https://api.elrond.com/collections/count');
  const maxPage = count && Math.ceil(count / size);

  const refresh = (page: number) => {
    setTransactionsActualPage(page);
    page -= 1;
    const requestSize = page * size > (count ?? 0) ? (count ?? 0) % (page * size) : size;
    request(`https://api.elrond.com/collections?from=${page * size}&size=${requestSize}`);
  };

  useEffect(() => {
    refresh(1);
  }, []);

  return [nftCollections, actualPage, maxPage, refresh, isLoading, hasError];
}

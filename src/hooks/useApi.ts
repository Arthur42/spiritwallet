import axios from 'axios';
import { useCallback, useEffect, useState } from 'react';

type UseApiReturn<T> = [T | undefined, boolean, boolean];
type UseLazyApiReturn<T> = [(link: string) => void, T | undefined, boolean, boolean];

/**
 * handle any Api with axios
 * @param {props} link Api url
 * @return {[data, isLoading, hasError]}
 */
export default function useApi<T>(link: string | undefined): UseApiReturn<T> {
  const [request, data, isLoading, hasError] = useLazyApi<T>();

  useEffect(() => {
    if (link) {
      request(link);
    }
  }, [link]);

  return [data, isLoading, hasError];
}

/**
 * handle any Api with axios
 * @return {[data, isLoading, hasError]}
 */
export function useLazyApi<T>(): UseLazyApiReturn<T> {
  const [data, setData] = useState<T>();
  const [hasError, setHasError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const sendRequest = useCallback((link: string) => {
    setIsLoading(true);
    axios
      .get<T>(link)
      .then((res) => {
        if (res.data) {
          console.debug('useApi response :', res.data);
          setData(res.data);
        }
        setIsLoading(false);
      })
      .catch((e) => {
        console.error('useApi error :', e);
        setHasError(true);
        setIsLoading(false);
      });
  }, []);

  return [sendRequest, data, isLoading, hasError];
}

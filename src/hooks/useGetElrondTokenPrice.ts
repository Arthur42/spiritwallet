import { ApolloError, gql, useQuery } from '@apollo/client';

const TOKEN_PRICE = gql`
  query GetTokenPriceUSD($tokenID: String!) {
    getTokenPriceUSD(tokenID: $tokenID)
  }
`;

/**
 * get elrond token price in usd
 * @param {props} tokenID (ex : MEX-455c57)
 * @return {[data, loading, error]}
 */
export default function useGetElrondTokenPrice(
  tokenID: string
): [string | undefined, boolean, ApolloError | undefined] {
  const { loading, error, data } = useQuery(TOKEN_PRICE, {
    variables: { tokenID }
  });

  return [data?.getTokenPriceUSD, loading, error];
}

import React from 'react';
import './App.css';
import Router from './router';
import Header from './components/data/Header';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import createTheme from '@mui/material/styles/createTheme';
import ThemeProvider from '@mui/material/styles/ThemeProvider';
import useTheme from '@mui/material/styles/useTheme';
import Footer from './components/data/Footer';
import useSelectedTheme from './hooks/useTheme';

const client = new ApolloClient({
  uri: 'https://graph.maiar.exchange/graphql',
  cache: new InMemoryCache()
});

const lightTheme = createTheme({
  palette: {
    mode: 'light',
    background: {
      default: '#f2f2f2'
    }
  }
});

const darkTheme = createTheme({
  palette: {
    mode: 'dark'
  }
});

/**
 * App
 * @constructor
 */
export default function App() {
  const [actualTheme, changeTheme] = useSelectedTheme();

  if (!actualTheme) {
    return null;
  }

  return (
    <div className="App">
      <ApolloProvider client={client}>
        <ThemeProvider theme={actualTheme === 'dark' ? darkTheme : lightTheme}>
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          />
          <Header changeTheme={changeTheme} />
          <Router />
          <Footer />
          <HandleBackgroundColor />
        </ThemeProvider>
      </ApolloProvider>
    </div>
  );
}

/**
 * change background color
 * @constructor
 */
function HandleBackgroundColor() {
  const theme = useTheme();

  return (
    <style>
      {`
        html {
          background-color: ${theme.palette.background.default};
        }
      `}
    </style>
  );
}

import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Box from '@mui/material/Box';
import Chip from '@mui/material/Chip';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import {
  delegatorLink,
  explorerLink,
  nftCollectionLink,
  tokenLink,
  transactionLink,
  walletLink
} from '../../utils/links';
import TabContext from '@mui/lab/TabContext';
import TabPanel from '@mui/lab/TabPanel';
import useApi from '../../hooks/useApi';
import { NftCollectionsItem } from '../../types/nftCollection';
import List from '../../components/data/List';
import ShrinkAddress from '../../components/text/address/ShrinkAddress';
import { TransactionsItem } from '../../types/transaction';
import getTransactionStatusColor from '../../utils/getTransactionStatusColor';
import TransactionValue from './TransactionValue';
import { TokensItem } from '../../types/token';
import LinkTo from '../../components/text/LinkTo';
import TokenValue from '../../components/text/TokenValue';
import { EGLD_IDENTIFIER, EGLD_ICON_URL, EGLD_NAME, DEFAULT_LIST_SIZE } from '../../utils/config';
import Identity from '../../types/identity';
import _ from 'lodash';
import useTransactions from '../../hooks/apiElrond/useTransactions';
import useTokens from '../../hooks/apiElrond/useTokens';
import useNftCollections from '../../hooks/apiElrond/useNftCollections';
import useAccounts from '../../hooks/apiElrond/useAccounts';
import { AccountsItem } from '../../types/account';
import toLowerAndSpaceCase from '../../utils/toLowerAndSpaceCase';
import MoreTransactionsComponent from '../../components/data/MoreTransactionsComponent';

const tabs = [
  {
    title: 'accounts'
  },
  {
    title: 'nfts'
  },
  {
    title: 'transactions'
  },
  {
    title: 'tokens'
  },
  {
    title: 'delegations'
  }
];

/**
 * display Explorer page
 * @constructor
 */
export default function Explorer() {
  const { section } = useParams();
  const navigate = useNavigate();
  // find category with params
  const tabIndex = tabs.findIndex((tab) => tab.title === section);
  // displayed category
  const [tabContext, setTabContext] = useState(tabIndex !== -1 ? tabIndex : 0);
  // region API
  const [accounts, accountsActualPage, accountsMaxPage, refreshAccounts, accountsLoading] =
    useAccounts();
  const [
    nftCollections,
    nftCollectionsActualPage,
    nftCollectionsMaxPage,
    refreshNftCollections,
    nftCollectionsLoading
  ] = useNftCollections();
  const [
    transactions,
    transactionsActualPage,
    transactionsMaxPage,
    refreshTransactions,
    transactionsLoading
  ] = useTransactions();
  const [tokens, tokensActualPage, tokensMaxPage, refreshTokens, tokensLoading] = useTokens();
  const [identities, identitiesLoading] = useApi<Identity[]>('https://api.elrond.com/identities');
  // endregion
  // identities pagination
  const [identitiesActualPage, setIdentitiesActualPage] = useState(1);
  // displayed identities
  const actualPageIdentities = useMemo(
    () =>
      _.filter(identities, (_, index) => {
        return (
          index >= (identitiesActualPage - 1) * DEFAULT_LIST_SIZE &&
          index < identitiesActualPage * DEFAULT_LIST_SIZE
        );
      }),
    [nftCollections, identitiesActualPage]
  );

  // set page title
  useEffect(() => {
    document.title = explorerLink.title(section);
  }, [section]);

  const More = useCallback(
    ({ item, displayed }: { item: TransactionsItem; displayed: boolean }) => {
      return <MoreTransactionsComponent item={item} displayed={displayed} />;
    },
    []
  );

  return (
    <>
      <TabContext value={tabContext.toString()}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs
            value={tabContext}
            onChange={(event, value) => {
              navigate(explorerLink.linkTo(tabs[value].title), { replace: false });
              setTabContext(value);
            }}
            variant="scrollable"
            scrollButtons
            allowScrollButtonsMobile>
            {_.map(tabs, (tab) => (
              <Tab label={tab.title} key={tab.title} />
            ))}
          </Tabs>
        </Box>
        <TabPanel value={'0'}>
          <List<AccountsItem>
            list={accounts ?? []}
            columnNames={['Address', 'Balance']}
            items={AccountsItems}
            itemsKeys={_.map(accounts, (c) => c.address)}
            navigation={{
              actualPage: accountsActualPage,
              maxPage: accountsMaxPage ?? 0,
              updatePage: refreshAccounts
            }}
            loading={accountsLoading}
          />
        </TabPanel>
        <TabPanel value={'1'}>
          <List<NftCollectionsItem>
            list={nftCollections ?? []}
            columnNames={['Name', 'Collection', 'Date', 'Owner']}
            items={NftCollectionItems}
            itemsKeys={_.map(nftCollections, (c) => c.ticker)}
            navigation={{
              actualPage: nftCollectionsActualPage,
              maxPage: nftCollectionsMaxPage ?? 0,
              updatePage: refreshNftCollections
            }}
            loading={nftCollectionsLoading}
          />
        </TabPanel>
        <TabPanel value={'2'}>
          <List<TransactionsItem>
            list={transactions ?? []}
            columnNames={['Hash', 'Description', 'From', 'To', 'Value', 'Status']}
            items={TransactionItems}
            itemsKeys={_.map(transactions, (t) => t.txHash)}
            navigation={{
              actualPage: transactionsActualPage,
              maxPage: transactionsMaxPage ?? 0,
              updatePage: refreshTransactions
            }}
            ShowMoreComponent={More}
            loading={transactionsLoading}
          />
        </TabPanel>
        <TabPanel value={'3'}>
          <List<TokensItem>
            list={tokens ?? []}
            columnNames={['Name', 'Description', 'Owner']}
            items={TokenItems}
            itemsKeys={_.map(tokens, (t) => t.ticker)}
            navigation={{
              actualPage: tokensActualPage,
              maxPage: tokensMaxPage ?? 0,
              updatePage: refreshTokens
            }}
            loading={tokensLoading}
          />
        </TabPanel>
        <TabPanel value={'4'}>
          <List<Identity>
            list={actualPageIdentities}
            columnNames={['Name', 'APR', 'Locked']}
            items={ProvidersItems}
            itemsKeys={_.map(actualPageIdentities, (i) => i.identity)}
            navigation={{
              actualPage: identitiesActualPage,
              maxPage: identities?.length ? Math.ceil(identities?.length / DEFAULT_LIST_SIZE) : 0,
              updatePage: setIdentitiesActualPage
            }}
            loading={identitiesLoading}
          />
        </TabPanel>
      </TabContext>
    </>
  );
}

const AccountsItems = (item: AccountsItem) => [
  <LinkTo key={`${item.address}1`} name={item.address} linkTo={walletLink.linkTo(item.address)} />,
  <TokenValue
    key={`${item.address}2`}
    name={EGLD_NAME}
    identifier={EGLD_IDENTIFIER}
    value={item.balance}
    fixed={2}
    tokenImageUrl={EGLD_ICON_URL}
  />
];

const NftCollectionItems = (item: NftCollectionsItem) => [
  <span key={`${item.ticker}1`}>{item.name}</span>,
  <LinkTo
    key={`${item.ticker}2`}
    name={item.collection}
    linkTo={nftCollectionLink.linkTo(item.collection)}
  />,
  <span key={`${item.ticker}3`}>{new Date(item.timestamp * 1000).toLocaleString()}</span>,
  <ShrinkAddress
    key={`${item.ticker}4`}
    address={item.owner}
    linkTo={walletLink.linkTo(item.owner)}
  />
];

const TransactionItems = (item: TransactionsItem) => [
  <ShrinkAddress
    key={`${item.txHash}1`}
    address={item.txHash}
    linkTo={transactionLink.linkTo(item.txHash)}
  />,
  <span key={`${item.txHash}2`}>{toLowerAndSpaceCase(item.function ?? '')}</span>,
  <ShrinkAddress
    key={`${item.txHash}3`}
    address={item.sender}
    linkTo={walletLink.linkTo(item.sender)}
  />,
  <ShrinkAddress
    key={`${item.txHash}4`}
    address={item.receiver}
    linkTo={walletLink.linkTo(item.receiver)}
  />,
  <TransactionValue key={`${item.txHash}5`} item={item} />,
  <Chip
    key={`${item.txHash}6`}
    label={item.status}
    color={getTransactionStatusColor(item.status)}
  />
];

const TokenItems = (item: TokensItem) => [
  <LinkTo key={`${item.ticker}1`} name={item.name} linkTo={tokenLink.linkTo(item.identifier)} />,
  <span key={`${item.ticker}2`}>{item.assets?.description}</span>,
  <ShrinkAddress
    key={`${item.ticker}3`}
    address={item.owner}
    linkTo={walletLink.linkTo(item.owner)}
  />
];

const ProvidersItems = (item: Identity) => [
  item.name.length > 30 ? (
    <ShrinkAddress
      key={`${item.identity}1`}
      address={item.name}
      linkTo={delegatorLink.linkTo(item.identity)}
    />
  ) : (
    <LinkTo
      key={`${item.identity}1`}
      name={item.name}
      linkTo={delegatorLink.linkTo(item.identity)}
    />
  ),
  <span key={`${item.identity}2`}>{item.apr ?? 0}%</span>,
  <TokenValue
    key={`${item.identity}3`}
    name={EGLD_NAME}
    identifier={EGLD_IDENTIFIER}
    tokenImageUrl={EGLD_ICON_URL}
    value={item.locked}
    fixed={2}
  />
];

import Transaction, {
  TransactionActionTransfers,
  TransactionsItem
} from '../../../types/transaction';
import TokenValue from '../../../components/text/TokenValue';
import _ from 'lodash';
import React from 'react';
import { EGLD_IDENTIFIER, EGLD_ICON_URL, EGLD_NAME } from '../../../utils/config';

/**
 * return transaction value
 * @param {props} item
 * @constructor
 */
export default function TransactionValue({ item }: { item: Transaction | TransactionsItem }) {
  if (parseInt(item.value)) {
    return (
      <span>
        <TokenValue
          name={EGLD_NAME}
          identifier={EGLD_IDENTIFIER}
          tokenImageUrl={EGLD_ICON_URL}
          value={item.value}
          fixed={2}
        />
      </span>
    );
  }
  if (
    item.action?.arguments &&
    'transfers' in item.action.arguments &&
    item.action.arguments.transfers
  ) {
    return (
      <>
        {_.map(item.action?.arguments.transfers, (transfer: TransactionActionTransfers) => (
          <span key={transfer.value + transfer.token}>
            <TokenValue
              name={transfer.token ?? transfer.collection ?? ''}
              identifier={transfer.token ?? transfer.collection ?? ''}
              value={transfer.value}
              fixed={2}
              decimals={parseInt(transfer.decimals)}
              tokenImageUrl={transfer.svgUrl}
            />
          </span>
        ))}
      </>
    );
  }
  return null;
}

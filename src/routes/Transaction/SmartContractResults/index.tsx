import React, { useState } from 'react';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import AccordionDetails from '@mui/material/AccordionDetails';
import _ from 'lodash';
import TokenValue from '../../../components/text/TokenValue';
import { EGLD_ICON_URL, EGLD_IDENTIFIER, EGLD_NAME } from '../../../utils/config';
import { TransactionResult } from '../../../types/transaction';
import styled from '@mui/material/styles/styled';
import Accordion, { AccordionProps } from '@mui/material/Accordion';
import AddressFromTo from '../../../components/text/address/AddressFromTo';
import PaperContainer from '../../../components/containers/PaperContainer';

export type SmartContractResultsProps = {
  scResults: TransactionResult[] | undefined;
};

/**
 * SmartContractResults
 * @param {props} scResults
 * @constructor
 */
export default function SmartContractResults({ scResults }: SmartContractResultsProps) {
  const [expanded, setExpanded] = useState(true);
  if (!scResults) {
    return null;
  }

  return (
    <MuiAccordion expanded={expanded} onChange={(event) => setExpanded((old) => !old)}>
      <AccordionSummary>
        <Typography>Smart contract results</Typography>
      </AccordionSummary>
      <AccordionDetails>
        {_.map(scResults, (result) => (
          <PaperContainer key={result.hash} elevation={8}>
            <AddressFromTo from={result.sender} to={result.receiver} />
            {result.value && result.value !== '0' && (
              <TokenValue
                prefix={'value\u00a0: '}
                name={EGLD_NAME}
                identifier={EGLD_IDENTIFIER}
                tokenImageUrl={EGLD_ICON_URL}
                value={result.value}
              />
            )}
            {result.data && (
              <PaperContainer elevation={2}>
                <Typography>{result.data}</Typography>
              </PaperContainer>
            )}
          </PaperContainer>
        ))}
      </AccordionDetails>
    </MuiAccordion>
  );
}

const MuiAccordion = styled((props: AccordionProps) => (
  <Accordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  '&:not(:last-child)': {
    borderBottom: 0
  },
  '&:before': {
    display: 'none'
  },
  'margin-top': '15px'
}));

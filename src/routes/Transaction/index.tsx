import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import useApi from '../../hooks/useApi';
import Accordion, { AccordionProps } from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Card from '@mui/material/Card';
import Chip from '@mui/material/Chip';
import Typography from '@mui/material/Typography';
import styled from '@mui/material/styles/styled';
import type TransactionType from '../../types/transaction';
import LinkTo from '../../components/text/LinkTo';
import { transactionLink, walletLink } from '../../utils/links';
import getTransactionStatusColor from '../../utils/getTransactionStatusColor';
import SmartContractResults from './SmartContractResults';
import Operations from './Operations';
import TokenValue from '../../components/text/TokenValue';
import { EGLD_ICON_URL, EGLD_IDENTIFIER, EGLD_NAME } from '../../utils/config';
import prettierNumber from '../../utils/prettierNumber';

/**
 * display elrond transaction
 * @constructor
 */
export default function Transaction() {
  const { transactionIdentifier } = useParams();
  const [transaction, isTransactionLoading, hasTransactionError] = useApi<TransactionType>(
    transactionIdentifier && `https://api.elrond.com/transactions/${transactionIdentifier}`
  );

  // set page title
  useEffect(() => {
    document.title = transactionLink.title(transactionIdentifier);
  }, [transactionIdentifier]);

  // loading
  if (isTransactionLoading) {
    return null;
  }

  // error
  if (hasTransactionError || !transaction) {
    return null;
  }

  // display transaction
  return (
    <MainContainer>
      <TransactionContainer>
        <TransactionStatusContainer>
          <Chip label={transaction.status} color={getTransactionStatusColor(transaction.status)} />
        </TransactionStatusContainer>
        <LinkTo
          prefix={'from\u00a0:'}
          name={transaction.sender}
          linkTo={walletLink.linkTo(transaction.sender)}
        />
        <LinkTo
          prefix={'to\u00a0:'}
          name={transaction.receiver}
          linkTo={walletLink.linkTo(transaction.receiver)}
        />
        <Typography>{`gaz\u00a0used\u00a0: ${prettierNumber(
          transaction.gasUsed.toString()
        )}`}</Typography>
        <TokenValue
          prefix={'price\u00a0: '}
          name={EGLD_NAME}
          identifier={EGLD_IDENTIFIER}
          tokenImageUrl={EGLD_ICON_URL}
          value={transaction.fee}
        />
        <MuiAccordion>
          <AccordionSummary>
            <Typography>More information</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <pre>{JSON.stringify(transaction.action, null, 2)}</pre>
          </AccordionDetails>
        </MuiAccordion>
        <SmartContractResults scResults={transaction.results} />
        <Operations operations={transaction.operations} />
      </TransactionContainer>
    </MainContainer>
  );
}

const MainContainer = styled('div')`
  display: flex;
  justify-content: center;
`;

const TransactionContainer = styled(Card)`
  position: relative;
  max-width: 800px;
  width: 100%;
  padding: 20px;
`;

const TransactionStatusContainer = styled('div')`
  position: absolute;
  padding-right: inherit;
  right: 0;
`;

const MuiAccordion = styled((props: AccordionProps) => (
  <Accordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  '&:not(:last-child)': {
    borderBottom: 0
  },
  '&:before': {
    display: 'none'
  },
  'margin-top': '15px'
}));

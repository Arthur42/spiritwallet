import React, { useState } from 'react';
import { TransactionOperation } from '../../../types/transaction';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import styled from '@mui/material/styles/styled';
import Accordion, { AccordionProps } from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import _ from 'lodash';
import TokenValue from '../../../components/text/TokenValue';
import { EGLD_ICON_URL, EGLD_IDENTIFIER, EGLD_NAME } from '../../../utils/config';
import AddressFromTo from '../../../components/text/address/AddressFromTo';
import PaperContainer from '../../../components/containers/PaperContainer';

export type OperationsProps = {
  operations: TransactionOperation[] | undefined;
};

/**
 * Operations
 * @param {props} operations
 * @constructor
 */
export default function Operations({ operations }: OperationsProps) {
  const [expanded, setExpanded] = useState(true);
  if (!operations) {
    return null;
  }

  return (
    <MuiAccordion expanded={expanded} onChange={(event) => setExpanded((old) => !old)}>
      <AccordionSummary>
        <Typography>Token operations</Typography>
      </AccordionSummary>
      <AccordionDetails>
        {_.map(operations, (operation) => (
          <PaperContainer key={operation.id} elevation={8}>
            <AddressFromTo from={operation.sender} to={operation.receiver} />
            {operation.value && (
              <TokenValue
                prefix={'value\u00a0: '}
                name={operation.name ?? EGLD_NAME}
                identifier={operation.identifier ?? EGLD_IDENTIFIER}
                tokenImageUrl={operation.type === 'egld' ? EGLD_ICON_URL : undefined}
                value={operation.value}
              />
            )}
            {operation.data && (
              <PaperContainer elevation={2}>
                <Typography>{operation.data}</Typography>
              </PaperContainer>
            )}
          </PaperContainer>
        ))}
      </AccordionDetails>
    </MuiAccordion>
  );
}

const MuiAccordion = styled((props: AccordionProps) => (
  <Accordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  '&:not(:last-child)': {
    borderBottom: 0
  },
  '&:before': {
    display: 'none'
  },
  'margin-top': '15px'
}));

import Token from '../../../types/token';
import { Chip } from '../../../components/data/BooleanChips';

/**
 * get chips values
 * @param {prop} token
 * @return {chips}
 */
export default function getChips(token: Token): Chip[] {
  return [
    {
      name: 'can upgrade',
      color: token.canUpgrade
    },
    {
      name: 'can pause',
      color: token.canPause
    },
    {
      name: 'can freeze',
      color: token.canFreeze
    },
    {
      name: 'can mint',
      color: token.canMint
    },
    {
      name: 'can change owner',
      color: token.canChangeOwner
    },
    {
      name: 'can burn',
      color: token.canBurn
    },
    {
      name: 'can wipe',
      color: token.canWipe
    }
  ];
}

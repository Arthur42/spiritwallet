import React, { useEffect } from 'react';
import styled from '@mui/material/styles/styled';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { useParams } from 'react-router-dom';
import useApi from '../../hooks/useApi';
import type TokenType from '../../types/token';
import BooleanChips from '../../components/data/BooleanChips';
import getChips from './utils/getChips';
import LinkTo from '../../components/text/LinkTo';
import prettierNumber from '../../utils/prettierNumber';
import useGetElrondTokenPrice from '../../hooks/useGetElrondTokenPrice';
import getPrice from '../../utils/getPrice';
import { tokenLink } from '../../utils/links';

/**
 * display Token page
 * @constructor
 */
export default function Token() {
  const { tokenIdentifier } = useParams();
  const [token, isTokenLoading, hasTokenError] = useApi<TokenType>(
    tokenIdentifier && `https://api.elrond.com/tokens/${tokenIdentifier}`
  );
  const [maiarExchangePrice] = useGetElrondTokenPrice(token?.identifier ?? '');

  // set page title
  useEffect(() => {
    if (token) {
      document.title = tokenLink.title(token.name);
    } else {
      document.title = tokenLink.title(tokenIdentifier);
    }
  }, [token, tokenIdentifier]);

  if (isTokenLoading || !token || hasTokenError) {
    return null;
  }

  return (
    <MainContainer>
      <TokenContainer>
        <Typography component="h1" variant="h5">
          {token.name} ({token.ticker})
        </Typography>
        <BooleanChips chips={getChips(token)} />
        <Container>
          <Typography component="h2" variant="h6">
            {'Description\u00a0:'}
          </Typography>
          {token.assets?.description && <Typography>{token.assets.description}</Typography>}
        </Container>
        <Container>
          <Typography component="h2" variant="h6">
            {'Information\u00a0:'}
          </Typography>
          <Typography>{`supply\u00a0: ${prettierNumber(token.supply)}`}</Typography>
          <Typography>{`circulating\u00a0supply\u00a0: ${prettierNumber(
            token.circulatingSupply
          )}`}</Typography>
          {maiarExchangePrice && parseInt(maiarExchangePrice) !== 0 && (
            <Typography>{`Maiar\u00a0Exchange\u00a0price\u00a0: ${getPrice(
              1,
              parseFloat(maiarExchangePrice)
            )}`}</Typography>
          )}
        </Container>
        <Container>
          <Typography component="h2" variant="h6">
            {'Links\u00a0:'}
          </Typography>
          {token.assets?.website && (
            <LinkTo name={token.assets.website} linkTo={token.assets.website} />
          )}
          {token.assets?.social?.twitter && (
            <LinkTo name={token.assets.social?.twitter} linkTo={token.assets.social?.twitter} />
          )}
          {token.assets?.social?.email && (
            <LinkTo name={token.assets.social?.email} linkTo={token.assets.social?.email} />
          )}
          {token.assets?.social?.blog && (
            <LinkTo name={token.assets.social?.blog} linkTo={token.assets.social?.blog} />
          )}
          {token.assets?.social?.whitepaper && (
            <LinkTo
              name={token.assets.social?.whitepaper}
              linkTo={token.assets.social?.whitepaper}
            />
          )}
        </Container>
      </TokenContainer>
    </MainContainer>
  );
}

const MainContainer = styled('div')`
  display: flex;
  justify-content: center;
`;

const TokenContainer = styled(Card)`
  max-width: 800px;
  padding: 20px;
`;

const Container = styled('div')`
  padding-top: 20px;
`;

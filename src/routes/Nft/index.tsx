import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import useApi from '../../hooks/useApi';
import NftType from '../../types/nft';
import styled from '@mui/material/styles/styled';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import NftMedia from '../../components/data/NftMedia';
import _ from 'lodash';
import { nftCollectionLink, nftLink, walletLink } from '../../utils/links';
import LinkTo from '../../components/text/LinkTo';

/**
 * display Nft
 * @constructor
 */
export default function Nft() {
  const { nftIdentifier } = useParams();
  const [nft, isNftLoading, hasNftError] = useApi<NftType>(
    nftIdentifier && `https://api.elrond.com/nfts/${nftIdentifier}`
  );

  // set page title
  useEffect(() => {
    if (nft) {
      document.title = nftLink.title(nft.name);
    } else {
      document.title = nftLink.title(nftIdentifier);
    }
  }, [nft, nftIdentifier]);

  if (isNftLoading || !nft || hasNftError) {
    return null;
  }

  return (
    <MainContainer>
      <NftContainer>
        <CardContent>
          <Typography component="h1" variant="h6">
            {nft.name}
          </Typography>
          {nft.owner && (
            <LinkTo
              prefix={'owner\u00a0: '}
              name={nft.owner}
              linkTo={walletLink.linkTo(nft.owner)}
            />
          )}
          <LinkTo
            prefix={'collection\u00a0: '}
            name={nft.collection}
            linkTo={nftCollectionLink.linkTo(nft.collection)}
          />
          {nft.royalties && <Typography>{`royalties\u00a0: ${nft.royalties}%`}</Typography>}
          <Typography>{`tags\u00a0: ${_.join(nft.tags, ', ')}`}</Typography>
        </CardContent>
        {nft.media?.map((media) => (
          <NftMedia media={media} key={media.url} />
        ))}
      </NftContainer>
    </MainContainer>
  );
}

const MainContainer = styled('div')({
  display: 'flex',
  justifyContent: 'center'
});

const NftContainer = styled(Card)({
  maxWidth: '800px',
  width: '100%'
});

import React, { useEffect, useState } from 'react';
import { Params, useParams } from 'react-router-dom';
import useApi from '../../hooks/useApi';
import Accordion, { AccordionProps } from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import NftCollectionType, { NftCollectionRole } from '../../types/nftCollection';
import styled from '@mui/material/styles/styled';
import _ from 'lodash';
import LinkTo from '../../components/text/LinkTo';
import { nftCollectionLink, walletLink } from '../../utils/links';
import Nft from '../../types/nft';
import Nfts from '../../components/data/Nfts';
import BooleanChips, { getBooleanChips } from '../../components/data/BooleanChips';
import useNfts from '../../hooks/apiElrond/useNfts';
import Skeleton from '@mui/material/Skeleton';

/**
 * display Nft
 * @constructor
 */
export default function NftCollection() {
  const params = useParams();
  const [{ nftCollectionIdentifier }, setParams] = useState<Readonly<Params>>({});
  const [nftCollection, isNftCollectionLoading] = useApi<NftCollectionType>(
    nftCollectionIdentifier && `https://api.elrond.com/collections/${nftCollectionIdentifier}`
  );

  // params value refresh state without changing value
  useEffect(() => {
    setParams(params);
  }, [params.id]);

  const [nfts, setNfts] = useState<Nft[]>([]);
  const [pageNfts, nftsActualPage, nftsMaxPage, nftsRefresh, nftsIsLoading] = useNfts<Nft>(
    'collections',
    nftCollectionIdentifier ?? '',
    ['NonFungibleESDT']
  );

  useEffect(() => {
    if (pageNfts) {
      setNfts(_.concat(nfts, pageNfts));
    }
  }, [pageNfts]);

  // set page title
  useEffect(() => {
    if (nftCollection) {
      document.title = nftCollectionLink.title(nftCollection.name);
    } else {
      document.title = nftCollectionLink.title(nftCollectionIdentifier);
    }
  }, [nftCollection, nftCollectionIdentifier]);

  return (
    <>
      <NftCollectionContainer>
        <NftCollectionCard>
          <CardContent>
            <Typography component="h1" variant="h6">
              {isNftCollectionLoading ? (
                <Skeleton animation="wave" width={200} />
              ) : (
                nftCollection?.name
              )}
            </Typography>
            <BooleanChips
              chips={nftCollection ? getBooleanChips(nftCollection) : []}
              isLoading={isNftCollectionLoading}
            />
            <LinkTo
              name={`xoxno`}
              linkTo={`https://xoxno.com/collection/${nftCollection?.collection}`}
            />
            <LinkTo
              name={`deadrare`}
              linkTo={`https://deadrare.io/collection/${nftCollection?.collection}`}
            />
            <MuiAccordion>
              <AccordionSummary>
                <Typography>Roles</Typography>
              </AccordionSummary>
              <AccordionDetails>
                {isNftCollectionLoading ? (
                  <RoleContainer>
                    <Skeleton animation="wave" width={200} />
                    <Skeleton animation="wave" width={200} />
                    <BooleanChips chips={[]} isLoading={true} />
                  </RoleContainer>
                ) : (
                  _.map(nftCollection?.roles, (role: NftCollectionRole) => (
                    <RoleContainer key={role.address}>
                      <LinkTo name={role.address} linkTo={walletLink.linkTo(role.address)} />
                      <Typography>
                        role{role.roles.length > 1 ? 's' : ''} : {role.roles.join(', ')}
                      </Typography>
                      <BooleanChips chips={getBooleanChips(role)} />
                    </RoleContainer>
                  ))
                )}
              </AccordionDetails>
            </MuiAccordion>
          </CardContent>
        </NftCollectionCard>
      </NftCollectionContainer>
      <Nfts
        nfts={nfts}
        actualPage={nftsActualPage}
        maxPage={nftsMaxPage ?? 0}
        isLoading={nftsIsLoading}
        refresh={nftsRefresh}
      />
    </>
  );
}

const NftCollectionContainer = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  paddingBottom: '30px'
});

const NftCollectionCard = styled(Card)({
  maxWidth: '800px',
  width: '100%',
  margin: '0 50px',
  padding: '5px'
});

const MuiAccordion = styled((props: AccordionProps) => (
  <Accordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  '&:not(:last-child)': {
    borderBottom: 0
  },
  '&:before': {
    display: 'none'
  },
  'margin-top': '15px'
}));

const RoleContainer = styled(Paper)({
  padding: '10px'
});

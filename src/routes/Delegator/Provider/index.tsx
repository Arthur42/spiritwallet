import useApi from '../../../hooks/useApi';
import ProviderType from '../../../types/provider';
import Typography from '@mui/material/Typography';
import React from 'react';
import LinkTo from '../../../components/text/LinkTo';
import { walletLink } from '../../../utils/links';
import prettierNumber from '../../../utils/prettierNumber';

/**
 * display delegator provider
 * @param {props} providerAddress
 * @constructor
 */
export default function Provider({ providerAddress }: { providerAddress: string }) {
  const [provider] = useApi<ProviderType>(`https://api.elrond.com/providers/${providerAddress}`);

  if (!provider) {
    return null;
  }

  return (
    <>
      <Typography component="h2" variant="h6">
        Delegation contracts
      </Typography>
      <LinkTo name={providerAddress} linkTo={walletLink.linkTo(providerAddress)} />
      <Typography>{`users\u00a0: ${prettierNumber(provider.numUsers.toString())}`}</Typography>
    </>
  );
}

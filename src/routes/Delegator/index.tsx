import React, { useEffect } from 'react';
import useApi from '../../hooks/useApi';
import styled from '@mui/styled-engine';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import { useParams } from 'react-router-dom';
import Identity from '../../types/identity';
import CardMedia from '@mui/material/CardMedia';
import Provider from './Provider';
import TokenValue from '../../components/text/TokenValue';
import { EGLD_ICON_URL, EGLD_IDENTIFIER, EGLD_NAME } from '../../utils/config';
import Link from '@mui/material/Link';
import { delegatorLink } from '../../utils/links';

/**
 * Display delegator page
 * @constructor
 */
export default function Delegator() {
  const { delegatorIdentity } = useParams();
  const [identity] = useApi<Identity>(`https://api.elrond.com/identities/${delegatorIdentity}`);

  // set page title
  useEffect(() => {
    if (identity) {
      document.title = delegatorLink.title(identity.name);
    } else {
      document.title = delegatorLink.title(delegatorIdentity);
    }
  }, [identity, delegatorIdentity]);

  if (!identity) {
    return null;
  }

  return (
    <MainContainer>
      <CardContainer>
        <Typography component="h3" variant="h6">
          <MediaContainer>
            <CardMedia src={identity.avatar} component="img" />
          </MediaContainer>
          <Link href={identity.website}>{identity.name}</Link>
        </Typography>
        <Typography>{`APR\u00a0: ${identity.apr}`}%</Typography>
        <TokenValue
          prefix={'locked\u00a0: '}
          name={EGLD_NAME}
          identifier={EGLD_IDENTIFIER}
          tokenImageUrl={EGLD_ICON_URL}
          value={identity.locked}
          fixed={2}
        />
      </CardContainer>
      <CardContainer>
        {identity.providers.map((providerAddress) => (
          <Provider key={providerAddress} providerAddress={providerAddress}></Provider>
        ))}
      </CardContainer>
    </MainContainer>
  );
}

const MainContainer = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const CardContainer = styled(Card)`
  max-width: 800px;
  width: 100%;
  padding: 20px;
  margin: 20px;
`;

const MediaContainer = styled('div')`
  display: inline-block;
  width: 15px;
  height: 15px;
  padding: 0 2px 0 5px;
`;

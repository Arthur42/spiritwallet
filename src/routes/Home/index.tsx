import React from 'react';
import './index.css';

/**
 * Home page
 * @constructor
 */
export default function Home() {
  return (
    <div className="scene">
      <div className="upper">
        <div className="moon">
          <div className="crater1"></div>
          <div className="crater2"></div>
        </div>
        <div className="star1"></div>
        <div className="star2"></div>
        <div className="star3"></div>
      </div>
    </div>
  );
}

import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Error404 from './Error404';
import { errorLink } from '../../utils/links';

/**
 * redirect to corresponding error
 * @constructor
 */
export default function Error() {
  const { error } = useParams();

  // set page title
  useEffect(() => {
    document.title = errorLink.title(error);
  }, [error]);

  switch (parseInt(error ?? '')) {
    default:
      return <Error404 />;
  }
}

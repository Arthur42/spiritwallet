import React from 'react';

/**
 * display error 404
 * @constructor
 */
export default function Error404() {
  return <p>Error 404</p>;
}

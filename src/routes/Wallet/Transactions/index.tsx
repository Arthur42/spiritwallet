import React, { useCallback } from 'react';
import TransactionType, { TransactionActionTransfer } from '../../../types/transaction';
import List from '../../../components/data/List';
import Typography from '@mui/material/Typography';
import { EGLD_ICON_URL, EGLD_IDENTIFIER, EGLD_NAME } from '../../../utils/config';
import TokenValue from '../../../components/text/TokenValue';
import _ from 'lodash';
import ShrinkAddress from '../../../components/text/address/ShrinkAddress';
import { transactionLink } from '../../../utils/links';
import Chip from '@mui/material/Chip';
import getTransactionStatusColor from '../../../utils/getTransactionStatusColor';
import useAccountTransactions from '../../../hooks/apiElrond/useAccountTransactions';
import MoreTransactionsComponent from '../../../components/data/MoreTransactionsComponent';

type TokensProps = {
  walletAddress: string;
};

/**
 * display wallet transactions
 * @param {props} walletAddress
 * @constructor
 */
export default function Transactions({ walletAddress }: TokensProps) {
  const [
    transactions,
    transactionsActualPage,
    transactionsMaxPage,
    refreshTransactions,
    transactionsLoading
  ] = useAccountTransactions(walletAddress);

  const transactionItems = (item: TransactionType) => {
    const title =
      item.action?.name?.replace(/[A-Z]/g, (text: string) => {
        return ' ' + text.toLowerCase();
      }) ?? (item.sender === walletAddress ? `send ${EGLD_NAME}` : `recieve ${EGLD_NAME}`);
    const description = item?.action?.description ?? '';
    return transactionItem(item, title, description);
  };

  const More = useCallback(
    ({ item, displayed }: { item: TransactionType; displayed: boolean }) => {
      return <MoreTransactionsComponent item={item} displayed={displayed} />;
    },
    [walletAddress]
  );

  return (
    <List<TransactionType>
      list={transactions ?? []}
      columnNames={['Hash', 'Title', 'Description', 'Value', 'Date', 'Status']}
      items={transactionItems}
      itemsKeys={_.map(transactions, (t) => t.txHash)}
      ShowMoreComponent={More}
      navigation={{
        actualPage: transactionsActualPage,
        maxPage: transactionsMaxPage ?? 0,
        updatePage: refreshTransactions
      }}
      loading={transactionsLoading}
    />
  );
}

const transactionItem = (transaction: TransactionType, title: string, description: string) => {
  return [
    <ShrinkAddress
      key={`${transaction.txHash}1`}
      address={transaction.txHash}
      linkTo={transactionLink.linkTo(transaction.txHash)}
      linkToOriginal={transactionLink.linkToOriginal(transaction.txHash)}
    />,
    <Typography key={`${transaction.txHash}2`}>{title}</Typography>,
    <Typography key={`${transaction.txHash}3`}>{description}</Typography>,
    <div key={`${transaction.txHash}4`}>
      {transaction.value !== '0' && (
        <TokenValue
          name={EGLD_NAME}
          identifier={EGLD_IDENTIFIER}
          value={transaction.value}
          fixed={2}
          tokenImageUrl={EGLD_ICON_URL}
        />
      )}
      {(transaction.action?.arguments as TransactionActionTransfer)?.transfers?.map((transfer) => (
        <div key={transfer.ticker}>
          <TokenValue
            name={transfer.name ?? transfer.collection ?? ''}
            identifier={transfer.token ?? transfer.collection ?? ''}
            value={transfer.value}
            fixed={2}
            decimals={parseInt(transfer.decimals)}
            tokenImageUrl={transfer.svgUrl}
          />
        </div>
      ))}
    </div>,
    <Typography key={`${transaction.txHash}5`}>
      {new Date(transaction.timestamp * 1000).toLocaleString()}
    </Typography>,
    <Chip
      key={`${transaction.txHash}6`}
      label={transaction.status}
      color={getTransactionStatusColor(transaction.status)}
    />
  ];
};

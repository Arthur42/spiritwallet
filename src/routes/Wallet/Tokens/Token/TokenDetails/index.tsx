import React, { useRef, useState } from 'react';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import Popper from '@mui/material/Popper';
import _ from 'lodash';
import Link from '@mui/material/Link';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import Paper from '@mui/material/Paper';
import TokenValue from '../../../../../components/text/TokenValue';
import { UnlockSchedule } from '../../../../../types/nft';
import { Tooltip } from '@mui/material';
import Typography from '@mui/material/Typography';

export type TokenDetail = {
  identifier: string;
  balance: string;
  unlockSchedule?: UnlockSchedule[];
};

export type TokenDetailsProps = {
  details: TokenDetail[];
  iconUrl?: string;
};

// todo: implement unlockTimestamp
export default function TokenDetails({ details, iconUrl }: TokenDetailsProps) {
  const detailsRef = useRef(null);
  const [isDetailOpen, setIsDetailOpen] = useState<boolean>(false);

  return (
    <ClickAwayListener onClickAway={() => setIsDetailOpen(false)}>
      <div>
        <Popper open={isDetailOpen} anchorEl={detailsRef.current} placement={'top-start'}>
          <Paper elevation={3} sx={{ padding: '10px 15px' }}>
            {_.map(details, (detail) => (
              <Tooltip
                placement={'right-start'}
                arrow
                title={
                  detail.unlockSchedule ? (
                    <>
                      {_.map(detail.unlockSchedule, (schedule) => (
                        <Typography key={schedule.remainingEpochs + schedule.percent}>
                          unlock {schedule.percent}%{' '}
                          {schedule.remainingEpochs > 0
                            ? ' : ' +
                              new Date(
                                new Date().getTime() +
                                  schedule.remainingEpochs * 1000 * 60 * 60 * 24
                              ).toLocaleDateString()
                            : 'now'}
                        </Typography>
                      ))}
                    </>
                  ) : (
                    ''
                  )
                }>
                <div>
                  <TokenValue
                    name={detail.identifier}
                    identifier={detail.identifier}
                    value={detail.balance}
                    tokenImageUrl={iconUrl}
                    type={'metaESDT'}
                    fixed={2}
                  />
                </div>
              </Tooltip>
            ))}
          </Paper>
        </Popper>
        <Link color={'inherit'} ref={detailsRef} onClick={() => setIsDetailOpen((old) => !old)}>
          <InfoOutlinedIcon
            fontSize={'small'}
            sx={{ position: 'absolute', right: '10px', top: '10px' }}
          />
        </Link>
      </div>
    </ClickAwayListener>
  );
}

import Card from '@mui/material/Card';
import CircularProgress from '@mui/material/CircularProgress';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import React from 'react';
import styled from '@mui/material/styles/styled';
import prettierNumber from '../../../../utils/prettierNumber';
import getPrice from '../../../../utils/getPrice';
import { tokenLink } from '../../../../utils/links';
import TokenDetails, { TokenDetail } from './TokenDetails';

export type TokenProps = {
  name: string;
  identifier: string;
  iconUrl?: string;
  balance: string;
  unitPrice?: number;
  isLoading?: boolean;
  displayError?: boolean;
  details?: TokenDetail[];
};

/**
 * display token basic card
 * @param {props} name token name
 * @param {props} identifier
 * @param {props} iconUrl token icon url
 * @param {props} balance token balance
 * @param {props} unitPrice token balance unit price
 * @param {props} isLoading
 * @param {props} displayError
 * @param {props} details
 * @constructor
 */
export default function Token({
  name,
  identifier,
  iconUrl,
  balance,
  unitPrice,
  isLoading,
  displayError,
  details
}: TokenProps) {
  if (isLoading) {
    return (
      <TokenContainer variant="outlined">
        <CircularProgress />
      </TokenContainer>
    );
  }

  if (displayError) {
    return (
      <TokenContainer variant="outlined">
        <Typography component="h3" variant="h6">
          ERROR
        </Typography>
      </TokenContainer>
    );
  }

  return (
    <TokenContainer variant="outlined">
      {details && details.length > 0 && <TokenDetails details={details} iconUrl={iconUrl} />}
      <Typography component="h3" variant="h6">
        {iconUrl && <TokenAssetsImage src={iconUrl} />}
        <Link href={tokenLink.linkTo(identifier)}>{name}</Link>
      </Typography>
      {balance && <TokensQuantity>{prettierNumber(balance)}</TokensQuantity>}
      {!!unitPrice && <Typography>{getPrice(parseFloat(balance), unitPrice)}</Typography>}
    </TokenContainer>
  );
}

const TokenContainer = styled(Card)`
  position: relative;
  width: 200px;
  height: 100px;
  padding: 20px;
  margin: 20px;
`;

const TokenAssetsImage = styled('img')`
  width: 15px;
  height: 15px;
  padding-right: 10px;
`;

const TokensQuantity = styled(Typography)`
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  margin-top: 5px;
`;

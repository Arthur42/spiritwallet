import React from 'react';
import styled from '@mui/material/styles/styled';
import type * as types from '../../../types/token';
import Nft, { MetaESDT as MetaESDTType } from '../../../types/nft';
import Token from './Token';
import calculateDecimal from '../../../utils/calculateDecimal';
import useApi from '../../../hooks/useApi';
import useGetElrondTokenPrice from '../../../hooks/useGetElrondTokenPrice';
import { EGLD_IDENTIFIER, EGLD_NAME } from '../../../utils/config';
import _ from 'lodash';

type TokensProps = {
  EGLDBalance: string;
  elrondTokens: types.Token[];
  metaESDTs: MetaESDTType[][];
};

/**
 * display elrond tokens
 * @param {props} EGLDBalance
 * @param {props} elrondTokens
 * @param {props} metaESDTs
 * @constructor
 */
export default function Tokens({ EGLDBalance, elrondTokens, metaESDTs }: TokensProps) {
  return (
    <TokensContainer>
      {EGLDBalance && <EGLDToken balance={EGLDBalance} />}
      {_.map(elrondTokens, (token) => (
        <ESDTToken token={token} key={token.owner + token.identifier} />
      ))}
      {_.map(metaESDTs, (nfts) => (
        <MetaESDT metaESDTs={nfts} key={nfts[0].identifier} />
      ))}
    </TokensContainer>
  );
}

type EGLDTokenProps = {
  balance: string;
};

/**
 * MetaESDT
 * @param {props} nft
 * @constructor
 */
function EGLDToken({ balance }: EGLDTokenProps) {
  const [APIPrice, isPriceLoading, hasPriceError] = useGetElrondTokenPrice(EGLD_IDENTIFIER);
  const [token, isTokenLoading, hasTokenError] = useApi<types.Token>(
    `https://api.elrond.com/tokens/${EGLD_IDENTIFIER}`
  );

  return (
    <Token
      name={EGLD_NAME}
      identifier={EGLD_IDENTIFIER}
      iconUrl={token?.assets?.pngUrl}
      balance={calculateDecimal(balance, token?.decimals)}
      unitPrice={parseFloat(APIPrice ?? '')}
      isLoading={isPriceLoading || isTokenLoading}
      displayError={!!hasPriceError || hasTokenError}
    />
  );
}

type MetaESDTProps = {
  metaESDTs: MetaESDTType[];
};

/**
 * MetaESDT
 * @param {props} metaESDTs
 * @constructor
 */
function MetaESDT({ metaESDTs }: MetaESDTProps) {
  const [token, isTokenLoading, hasTokenError] = useApi<types.Token>(
    `https://api.elrond.com/nfts/${metaESDTs[0].identifier}`
  );
  const balance: string = _.reduce<Nft, number>(
    metaESDTs,
    (acc, nft) => acc + parseInt(calculateDecimal(nft.balance, nft?.decimals)),
    0
  ).toString();
  const price: number = metaESDTs[0].price ?? 0;

  return (
    <Token
      name={token?.name ?? ''}
      identifier={token?.identifier ?? ''}
      iconUrl={token?.assets?.pngUrl}
      balance={balance}
      unitPrice={price}
      isLoading={isTokenLoading}
      displayError={hasTokenError}
      details={
        metaESDTs.length > 1
          ? _.map(metaESDTs, ({ identifier, balance, unlockSchedule }) => ({
              identifier,
              balance,
              unlockSchedule
            }))
          : undefined
      }
    />
  );
}

type TokenProps = {
  token: types.Token;
};

/**
 * MetaESDT
 * @param {props} token
 * @constructor
 */
function ESDTToken({ token }: TokenProps) {
  const [APIPrice, isPriceLoading, hasPriceError] = useGetElrondTokenPrice(token.identifier ?? '');

  return (
    <Token
      name={token.name}
      identifier={token.identifier}
      iconUrl={token.assets?.pngUrl}
      balance={calculateDecimal(token.balance, token.decimals)}
      unitPrice={parseFloat(APIPrice ?? '')}
      isLoading={isPriceLoading}
      displayError={!!hasPriceError}
    />
  );
}

const TokensContainer = styled('div')`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
`;

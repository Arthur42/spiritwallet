import React, { useEffect } from 'react';
import useApi from '../../hooks/useApi';
import Tokens from './Tokens';
import TabContext from '@mui/lab/TabContext';
import TabPanel from '@mui/lab/TabPanel';
import { useState } from 'react';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import { useNavigate, useParams } from 'react-router-dom';
import Transactions from './Transactions';
import Nft, { MetaESDT } from '../../types/nft';
import type Token from '../../types/token';
import Delegations from './Delegations';
import type Delegation from '../../types/delegation';
import type Account from '../../types/account';
import { walletLink } from '../../utils/links';
import AccountInformation from './AccountInformation';
import type { SmartContract } from '../../types/smartContract';
import _ from 'lodash';
import useNfts from '../../hooks/apiElrond/useNfts';
import Nfts from '../../components/data/Nfts';
const tabs = [
  {
    title: 'tokens'
  },
  {
    title: 'nfts'
  },
  {
    title: 'transactions'
  },
  {
    title: 'delegations'
  }
];

// force re rendering
export default function Wallet() {
  const { elrondWallet } = useParams();
  return <WalletC key={elrondWallet ?? ''} />;
}

/**
 * display elrond Wallet
 * @constructor
 */
export function WalletC() {
  const { elrondWallet, section } = useParams();
  const navigate = useNavigate();
  // search by wallet address
  const [walletAddress, setWalletAddress] = useState<string>();
  // search by username
  const [username, setUsername] = useState<string>();
  // displayed category
  const [tabContext, setTabContext] = useState<number>(0);
  // displayed Meta ESDT
  const [metaESDTs, setMetaESDTs] = useState<MetaESDT[][]>([]);
  // displayed nfts
  const [nfts, setNfts] = useState<Nft[]>([]);
  // region Api
  const [accountFindWithUsername] = useApi<Account>(
    username && `https://api.elrond.com/usernames/${username}`
  );
  const [account] = useApi<Account | SmartContract>(
    walletAddress && `https://api.elrond.com/accounts/${walletAddress}`
  );
  const [tokens] = useApi<Token[]>(
    walletAddress && `https://api.elrond.com/accounts/${walletAddress}/tokens`
  );
  const [metaNfts] = useNfts<MetaESDT>('accounts', walletAddress ?? '', ['MetaESDT'], 999);
  const [pageNfts, nftsActualPage, nftsMaxPage, nftsRefresh, nftsIsLoading] = useNfts<Nft>(
    'accounts',
    walletAddress ?? '',
    ['NonFungibleESDT', 'SemiFungibleESDT']
  );
  const [delegations] = useApi<Delegation[]>(
    walletAddress && `https://delegation-api.elrond.com/accounts/${walletAddress}/delegations`
  );
  // endregion

  // set page title
  useEffect(() => {
    document.title = walletLink.title(elrondWallet);
  }, [elrondWallet]);

  // find category with params
  useEffect(() => {
    if (section) {
      const tabIndex = tabs.findIndex((tab) => tab.title === section);
      setTabContext(tabIndex !== -1 ? tabIndex : 0);
    }
  }, [section]);

  // filter api NFTs
  useEffect(() => {
    if (pageNfts) {
      setNfts(_.concat(nfts, pageNfts));
    }
  }, [pageNfts]);

  useEffect(() => {
    if (metaNfts) {
      const metaESDTByIdentifier = _.reduce<MetaESDT, MetaESDT[][]>(
        metaNfts,
        (acc, esdt) => {
          let findInAcc = false;
          _.forEach(acc, (elem) => {
            if (elem[0].collection !== esdt.collection) return;
            elem.push(esdt);
            findInAcc = true;
            return false;
          });
          if (!findInAcc) acc.push([esdt]);
          return acc;
        },
        []
      );
      // save meta ESDTs group by collection
      setMetaESDTs(metaESDTByIdentifier);
    }
  }, [metaNfts]);

  // check if param is a wallet address or a username
  useEffect(() => {
    if (elrondWallet) {
      // if is an elrond wallet
      const regexWalletAddress = elrondWallet.match(/erd[a-z0-9]{59}/);
      if (regexWalletAddress?.length === 1 && regexWalletAddress[0] === elrondWallet) {
        // set wallet address and search user information
        setWalletAddress(elrondWallet);
      } else {
        // else check if it's an elrond username to find the wallet address
        setUsername(elrondWallet);
      }
    }
  }, [elrondWallet]);

  // if username is valid
  useEffect(() => {
    if (accountFindWithUsername?.address) {
      // set wallet address and search user information
      setWalletAddress(accountFindWithUsername.address);
    }
  }, [accountFindWithUsername]);

  return (
    <>
      <AccountInformation account={account} />
      <TabContext value={tabContext.toString()}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs
            value={tabContext}
            onChange={(event, value) => {
              navigate(walletLink.linkTo(username ?? walletAddress ?? '', tabs[value].title), {
                replace: false
              });
              setTabContext(value);
            }}
            variant="scrollable"
            scrollButtons
            allowScrollButtonsMobile>
            {_.map(tabs, (tab) => (
              <Tab label={tab.title} key={tab.title} />
            ))}
          </Tabs>
        </Box>
        <TabPanel value={'0'}>
          <Tokens
            EGLDBalance={account?.balance ?? ''}
            elrondTokens={tokens ?? []}
            metaESDTs={metaESDTs}
          />
        </TabPanel>
        <TabPanel value={'1'}>
          <Nfts
            nfts={nfts}
            actualPage={nftsActualPage}
            maxPage={nftsMaxPage ?? 0}
            isLoading={nftsIsLoading}
            refresh={nftsRefresh}
          />
        </TabPanel>
        <TabPanel value={'2'}>
          <Transactions walletAddress={walletAddress ?? ''} />
        </TabPanel>
        <TabPanel value={'3'}>
          <Delegations elrondDelegations={delegations ?? []} />
        </TabPanel>
      </TabContext>
    </>
  );
}

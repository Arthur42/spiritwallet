import React, { useState } from 'react';
import type Account from '../../../types/account';
import Card from '@mui/material/Card';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import styled from '@mui/material/styles/styled';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import type { SmartContract } from '../../../types/smartContract';
import Snackbar from '../../../components/data/Snackbar';

export type AccountInformationProps = {
  account: Account | SmartContract | undefined;
};

/**
 * display account information
 * @constructor
 */
export default function AccountInformation({ account }: AccountInformationProps) {
  const [isSnackbarOpen, setIsSnackbarOpen] = useState(false);

  if (!account) {
    return null;
  }

  return (
    <MainContainer>
      <InformationContainer>
        <Typography>
          {'ownerAddress' in account
            ? 'smart\u00a0contract'
            : `username\u00a0: ${account.username}`}
        </Typography>
        <div>
          <InlineText>{`address\u00a0: ${account.address}`}</InlineText>
          <Link
            onClick={() => {
              navigator.clipboard.writeText(account.address);
              setIsSnackbarOpen(true);
            }}>
            <ContentCopyIcon fontSize={'inherit'} />
          </Link>
        </div>
      </InformationContainer>
      <Snackbar
        isOpen={isSnackbarOpen}
        setIsOpen={setIsSnackbarOpen}
        message="copied to clipboard"
      />
    </MainContainer>
  );
}

const MainContainer = styled('div')`
  display: flex;
  justify-content: center;
  padding-bottom: 20px;
`;

const InformationContainer = styled(Card)`
  width: 80%;
  padding: 15px;
`;

const InlineText = styled(Typography)`
  display: inline-block;
`;

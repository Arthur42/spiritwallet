import React from 'react';
import useApi from '../../../../hooks/useApi';
import Typography from '@mui/material/Typography';
import prettierNumber from '../../../../utils/prettierNumber';
import calculateDecimal from '../../../../utils/calculateDecimal';
import styled from '@mui/material/styles/styled';
import Card from '@mui/material/Card';
import Provider from '../../../../types/provider';
import DelegationType from '../../../../types/delegation';
import Identity from '../../../../types/identity';
import LinkTo from '../../../../components/text/LinkTo';
import { delegatorLink, walletLink } from '../../../../utils/links';
import Link from '@mui/material/Link';

export type DelegationProps = {
  delegation: DelegationType;
};

/**
 * display a delegation
 * @param {props} delegatorAddress
 * @constructor
 */
export default function Delegation({ delegation }: DelegationProps) {
  const [provider] = useApi<Provider>(`https://api.elrond.com/providers/${delegation.contract}`);
  const [identity] = useApi<Identity>(
    provider && `https://api.elrond.com/identities/${provider.identity}`
  );

  if (!provider || !identity) {
    return null;
  }

  return (
    <MainContainer variant="outlined">
      <Link href={delegatorLink.linkTo(identity.identity)}>
        <Typography component="h3" variant="h6">
          {identity.name}
        </Typography>
      </Link>
      <LinkTo name={delegation.contract} linkTo={walletLink.linkTo(delegation.contract)} />
      <Typography>
        {'stacking\u00a0: ' + prettierNumber(calculateDecimal(delegation.userActiveStake))}
      </Typography>
      <Typography>
        {'rewards\u00a0: ' + prettierNumber(calculateDecimal(delegation.claimableRewards))}
      </Typography>
    </MainContainer>
  );
}

const MainContainer = styled(Card)`
  margin: 20px;
  padding: 20px;
`;

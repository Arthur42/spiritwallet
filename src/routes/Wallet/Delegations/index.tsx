import React from 'react';
import type * as types from '../../../types/delegation';
import Delegation from './Delegation';

type TokensProps = {
  elrondDelegations: types.Delegation[];
};

/**
 * display elrond tokens
 * @param {props} elrondTokens elrond tokens
 * @constructor
 */
export default function Delegations({ elrondDelegations }: TokensProps) {
  return (
    <>
      {elrondDelegations.map((delegation) => (
        <Delegation delegation={delegation} key={delegation.contract} />
      ))}
    </>
  );
}

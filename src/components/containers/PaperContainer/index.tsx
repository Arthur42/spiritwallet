import React from 'react';
import styled from '@mui/material/styles/styled';
import Paper, { PaperProps } from '@mui/material/Paper';

const PaperContainer = styled((props: PaperProps) => <Paper {...props} />)`
  padding: 15px;
  margin: 10px 0;
  border: 1px solid #444;
  overflow: auto;
`;

export default PaperContainer;

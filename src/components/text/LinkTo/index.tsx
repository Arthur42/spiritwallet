import React from 'react';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import styled from '@mui/material/styles/styled';

type LinkToWithOriginalProps = {
  prefix?: string | JSX.Element;
  name: string;
  suffix?: string | JSX.Element;
  linkTo: string;
};

/**
 * display to link, one on the site and the other on the original site
 * @param {props} prefix
 * @param {props} name
 * @param {props} suffix
 * @param {props} linkTo
 * @constructor
 */
export default function LinkTo({ prefix, name, suffix, linkTo }: LinkToWithOriginalProps) {
  return (
    <Container>
      {prefix && typeof prefix === 'string' ? (
        <Typography sx={{ paddingRight: '5px' }}>{prefix}</Typography>
      ) : (
        prefix
      )}
      <Link href={linkTo}>
        <Typography>{name}</Typography>
      </Link>
      {suffix && typeof suffix === 'string' ? (
        <Typography sx={{ paddingLeft: '5px' }}>{suffix}</Typography>
      ) : (
        suffix
      )}
    </Container>
  );
}

const Container = styled('div')`
  display: flex;
`;

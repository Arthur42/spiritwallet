import prettierNumber from '../../../utils/prettierNumber';
import calculateDecimal from '../../../utils/calculateDecimal';
import CardMedia from '@mui/material/CardMedia';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import React from 'react';
import styled from '@mui/material/styles/styled';
import { nftLink, tokenLink } from '../../../utils/links';

export type TokenValueProps = {
  name: string;
  prefix?: string;
  identifier: string;
  value: string;
  fixed?: number;
  decimals?: number;
  tokenImageUrl?: string;
  type?: 'ESDT' | 'metaESDT';
};

/**
 * display token with value
 * @param {props} name
 * @param {props} prefix
 * @param {props} identifier
 * @param {props} value
 * @param {props} fixed
 * @param {props} decimals
 * @param {props} tokenImageUrl
 * @param {props} type
 * @constructor
 */
export default function TokenValue({
  name,
  prefix,
  identifier,
  value,
  fixed,
  decimals,
  tokenImageUrl,
  type = 'ESDT'
}: TokenValueProps) {
  return (
    <Container>
      <Typography>
        {(prefix ?? '') + prettierNumber(calculateDecimal(value, decimals), fixed)}
      </Typography>
      {tokenImageUrl ? (
        <MediaContainer>
          <CardMedia src={tokenImageUrl} component="img" />
        </MediaContainer>
      ) : (
        <NoMedia />
      )}
      <Link href={type === 'ESDT' ? tokenLink.linkTo(identifier) : nftLink.linkTo(identifier)}>
        {name}
      </Link>
    </Container>
  );
}

const Container = styled('div')`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const MediaContainer = styled('div')`
  width: 22px;
  height: 22px;
  padding: 0 2px 0 5px;
`;

const NoMedia = styled('div')`
  width: 5px;
`;

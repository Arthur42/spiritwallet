import React from 'react';
import LinkTo from '../../LinkTo';

export type ShrinkAddressProps = {
  prefix?: string | JSX.Element;
  address: string;
  linkTo: string;
  linkToOriginal?: string;
};

/**
 * shrink address
 * @constructor
 * @param {props} prefix
 * @param {props} address
 * @param {props} linkTo
 * @param {props} linkToOriginal
 */
export default function ShrinkAddress({
  prefix,
  address,
  linkTo,
  linkToOriginal
}: ShrinkAddressProps) {
  return (
    <LinkTo
      prefix={prefix}
      name={`${address.slice(0, 7)}...${address.slice(address.length - 7, address.length)}`}
      linkTo={linkTo}
    />
  );
}

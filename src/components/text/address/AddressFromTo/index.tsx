import React from 'react';
import Typography from '@mui/material/Typography';
import ShrinkAddress from '../ShrinkAddress';
import { walletLink } from '../../../../utils/links';
import styled from '@mui/material/styles/styled';

export type AddressFromToProps = {
  from: string;
  to: string;
};

/**
 * AddressFromTo
 * @param {props} from
 * @param {props} to
 * @constructor
 */
export default function AddressFromTo({ from, to }: AddressFromToProps) {
  return (
    <InlineContainer>
      <Typography>{'from\u00a0:'}</Typography>
      <ShrinkAddress address={from} linkTo={walletLink.linkTo(from)} />
      <Typography>{'to\u00a0:'}</Typography>
      <ShrinkAddress address={to} linkTo={walletLink.linkTo(to)} />
    </InlineContainer>
  );
}

const InlineContainer = styled('div')`
  display: flex;
  gap: 5px;
`;

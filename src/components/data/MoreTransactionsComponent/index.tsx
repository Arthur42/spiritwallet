import React, { useEffect, useState } from 'react';
import { useLazyApi } from '../../../hooks/useApi';
import TransactionType, { TransactionsItem } from '../../../types/transaction';
import TransactionShowMore from '../TransactionShowMore';

export type MoreTransactionsComponentProps = {
  item: TransactionsItem;
  displayed: boolean;
};

/**
 * MoreTransactionsComponent
 * @constructor
 */
export default function MoreTransactionsComponent({
  item,
  displayed
}: MoreTransactionsComponentProps) {
  const [isLoading, setIsLoading] = useState(true);
  const [request, transaction, isTransactionLoading, hasTransactionError] =
    useLazyApi<TransactionType>();
  useEffect(() => {
    if (displayed && !transaction && isLoading && !isTransactionLoading && !hasTransactionError) {
      request(`https://api.elrond.com/transactions/${item.txHash}`);
    }
  }, [displayed]);
  useEffect(() => {
    if (!isTransactionLoading && (transaction || hasTransactionError)) {
      setIsLoading(false);
    }
  }, [isTransactionLoading, transaction, hasTransactionError]);
  return <TransactionShowMore transaction={transaction} isLoading={isLoading} />;
}

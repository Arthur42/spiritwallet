import React from 'react';
import styled from '@mui/material/styles/styled';
import type * as types from '../../../types/nft';
import Card from '@mui/material/Card';
import CardActionArea from '@mui/material/CardActionArea';
import Typography from '@mui/material/Typography';
import CardContent from '@mui/material/CardContent';
import { nftLink } from '../../../utils/links';
import NftMedia from '../NftMedia';
import _ from 'lodash';
import LoadingButton from '@mui/lab/LoadingButton';

type NftsProps = {
  nfts: types.Nft[];
  actualPage: number;
  maxPage: number;
  isLoading: boolean;
  refresh: (page: number) => void;
};

/**
 * display nfts
 * @param {props} nfts
 * @param {props} actualPage
 * @param {props} maxPage
 * @param {props} isLoading
 * @param {props} refresh
 * @constructor
 */
export default function Nfts({ nfts, actualPage, maxPage, isLoading, refresh }: NftsProps) {
  return (
    <>
      <NftsContainer>
        {_.map(nfts, (nft) => (
          <NftContainer key={nft.identifier}>
            <LinkContainer href={nftLink.linkTo(nft.identifier)}>
              <NftContentContainer>
                <Typography component="h3" variant="h6">
                  {nft.name}
                </Typography>
              </NftContentContainer>
              {_.map(nft.media, (media) => (
                <NftMedia media={media} key={media.url} />
              ))}
            </LinkContainer>
          </NftContainer>
        ))}
      </NftsContainer>
      {actualPage < maxPage ? (
        <LoadingButton
          variant="contained"
          size="large"
          loading={isLoading}
          onClick={() => !isLoading && refresh(actualPage + 1)}
          sx={{ display: 'block', margin: 'auto' }}>
          Show More
        </LoadingButton>
      ) : null}
    </>
  );
}

const NftsContainer = styled('div')`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
  text-align: center;
`;

const NftContainer = styled(Card)`
  height: fit-content;
  width: 200px;
  margin: 20px;
`;

const LinkContainer = styled(CardActionArea)<{ href: string }>`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
`;

const NftContentContainer = styled(CardContent)({
  padding: '5px 10px'
});

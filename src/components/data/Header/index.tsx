import React, { KeyboardEventHandler, useState } from 'react';
import Link from '@mui/material/Link';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import useTheme from '@mui/material/styles/useTheme';
import styled from '@mui/material/styles/styled';
import { useNavigate } from 'react-router-dom';
import DarkModeOutlinedIcon from '@mui/icons-material/DarkModeOutlined';
import LightModeOutlinedIcon from '@mui/icons-material/LightModeOutlined';
import SearchIcon from '@mui/icons-material/Search';
import { explorerLink, homeLink, walletLink } from '../../../utils/links';
import IconButton from '@mui/material/IconButton';

/**
 * application header
 * @constructor
 */
export default function Header({ changeTheme }: { changeTheme: () => void }) {
  const navigate = useNavigate();

  const [searchValue, setSearchValue] = useState<string>('');

  // on search key press
  const onTextFieldKeyPress: KeyboardEventHandler<HTMLDivElement> = (event) => {
    // if search key press is enter
    if (event.key === 'Enter') {
      // navigate to elrond wallet
      navigate(walletLink.linkTo(searchValue));
    }
  };

  const theme = useTheme();

  return (
    <HeaderContainer>
      <LeftContainer>
        <Link href={homeLink.linkTo} color="inherit" underline="none" sx={{ marginRight: '30px' }}>
          <Typography component="h2" variant="h6" width="fit-content">
            HOME
          </Typography>
        </Link>
        <Link
          href={explorerLink.linkTo()}
          color="inherit"
          underline="none"
          sx={{ marginRight: '30px' }}>
          <Typography component="h2" variant="h6" width="fit-content">
            EXPLORER
          </Typography>
        </Link>
      </LeftContainer>
      <RightContainer>
        <TextField
          label="herotag / erd address"
          size="small"
          onKeyDown={onTextFieldKeyPress}
          value={searchValue}
          onChange={(event) => setSearchValue(event.target.value)}
          InputProps={{
            endAdornment: <SearchIcon />
          }}
        />
        <Link component="a" sx={{ marginLeft: 2 }} onClick={() => changeTheme()}>
          <IconButton>
            {theme.palette.mode === 'dark' ? <DarkModeOutlinedIcon /> : <LightModeOutlinedIcon />}
          </IconButton>
        </Link>
      </RightContainer>
    </HeaderContainer>
  );
}

const HeaderContainer = styled('header')`
  display: flex;
  justify-content: space-between;
  min-height: 75px;
  margin: 15px 50px;
`;

const LeftContainer = styled('div')(
  (props) => `
  display: flex;
  align-items: center;
  color: ${props.theme.palette.text.primary}
`
);

const RightContainer = styled('div')`
  display: flex;
  align-items: center;
`;

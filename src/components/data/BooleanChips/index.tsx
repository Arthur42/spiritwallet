import React from 'react';
import styled from '@mui/material/styles/styled';
import Chip from '@mui/material/Chip';
import _ from 'lodash';
import Skeleton from '@mui/material/Skeleton';

export type BooleanChipsProps = {
  chips: Chip[];
  isLoading?: boolean;
};

export type Chip = {
  name: string;
  color: boolean;
};

/**
 * display boolean chips
 * @param {props} chips
 * @param {props} isLoading
 * @constructor
 */
export default function BooleanChips({ chips, isLoading }: BooleanChipsProps) {
  return (
    <BooleanChipContainer>
      {isLoading
        ? _.map(Array(3), (_, index) => (
            <Skeleton
              key={index.toString()}
              animation="wave"
              width={100}
              sx={{ display: 'inline-block', margin: '5px 5px 0 5px' }}
            />
          ))
        : _.map(chips, (chip) => (
            <Chip
              key={chip.name}
              label={chip.name}
              color={chip.color ? 'success' : 'error'}
              variant="outlined"
              sx={{ margin: '5px 5px 0 5px' }}
            />
          ))}
    </BooleanChipContainer>
  );
}

/**
 * getBooleanChip
 * @param {prop} params
 * @return {Chip[]}
 */
export function getBooleanChips(params: { [key: string]: unknown }): Chip[] {
  return _.reduce<{ [key: string]: unknown }, Chip[]>(
    params,
    (acc, value: unknown, key: string) => {
      if (typeof value === 'boolean' && _.startsWith(key, 'can')) {
        acc.push({
          name: _.replace(key, /([A-Z])/g, ' $1').toLowerCase(),
          color: value
        });
      }
      return acc;
    },
    []
  );
}

const BooleanChipContainer = styled('div')({
  margin: '10px 0'
});

import type * as types from '../../../types/nft';
import CardMedia from '@mui/material/CardMedia';
import React from 'react';
import styled from '@mui/material/styles/styled';

/**
 * return nft media
 * @param {props} media elrond media
 * @constructor
 */
export default function NftMedia({ media }: { media: types.NftMedia }) {
  if (media.fileType.startsWith('image/')) {
    return <Image src={media.url} />;
  }
  if (media.fileType.startsWith('video/')) {
    return <Image src={media.thumbnailUrl} />;
  }
  return null;
}

const Image = styled((props: { src: string }) => <CardMedia component="img" {...props} />)`
  &:hover {
    transform: scale(1.05, 1.05);
  }
  &:hover:after {
    transform: scale(1, 1);
  }
  transition: transform 100ms ease-in-out;
`;

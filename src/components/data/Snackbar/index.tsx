import React from 'react';
import IconButton from '@mui/material/IconButton';
import MuiSnackbar from '@mui/material/Snackbar';
import CloseIcon from '@mui/icons-material/Close';

export type SnackbarProps = {
  isOpen: boolean;
  setIsOpen: (open: boolean) => void;
  message: string;
};

/**
 * Snackbar message
 * @param {props} isOpen
 * @param {props} setIsOpen
 * @param {props} message
 * @constructor
 */
export default function Snackbar({ isOpen, setIsOpen, message }: SnackbarProps) {
  const handleClose = (event: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setIsOpen(false);
  };

  const action = (
    <React.Fragment>
      <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  return (
    <MuiSnackbar
      open={isOpen}
      autoHideDuration={6000}
      onClose={handleClose}
      message={message}
      action={action}
    />
  );
}

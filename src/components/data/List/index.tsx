import React, { useEffect, useState } from 'react';
import _ from 'lodash';
import Table from '@mui/material/Table';
import Paper, { PaperProps } from '@mui/material/Paper';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import styled from '@mui/material/styles/styled';
import IconButton from '@mui/material/IconButton';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import Link from '@mui/material/Link';
import Collapse from '@mui/material/Collapse';
import Typography from '@mui/material/Typography';
import { ArrowBackIos, ArrowForwardIos } from '@mui/icons-material';
import prettierNumber from '../../../utils/prettierNumber';
import Skeleton from '@mui/material/Skeleton';
import { DEFAULT_LIST_SIZE } from '../../../utils/config';
import TextField from '@mui/material/TextField';

export type ListProps<T> = {
  list: T[];
  columnNames: string[];
  items: (item: T) => JSX.Element[];
  ShowMoreComponent?: React.FC<{ item: T; displayed: boolean }>;
  itemsKeys: string[];
  loading?: boolean;
  loadingSize?: number;
  navigation?: {
    actualPage: number;
    maxPage: number;
    updatePage: (page: number) => void;
  };
};

/**
 * display a list with pagination
 * @constructor
 * @todo pagination
 * @todo add entry key
 */
export default function List<T>({
  list,
  columnNames,
  items,
  ShowMoreComponent,
  itemsKeys,
  loading,
  loadingSize = DEFAULT_LIST_SIZE,
  navigation
}: ListProps<T>) {
  return (
    <>
      <ListNavigation
        actualPage={navigation?.actualPage}
        maxPage={navigation?.maxPage}
        updatePage={navigation?.updatePage}
      />
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              {_.map(columnNames, (columnName, index) => (
                <TableCell key={columnName + index.toString()}>{columnName}</TableCell>
              ))}
              {ShowMoreComponent && <TableCell>Details</TableCell>}
            </TableRow>
          </TableHead>
          <TableBody>
            {!loading
              ? _.map(list, (item, rowIndex) => {
                  const itemsJSX = items(item);
                  return (
                    <Row<T>
                      key={itemsKeys[rowIndex]}
                      columnNames={columnNames}
                      ShowMoreComponent={ShowMoreComponent}
                      item={item}
                      itemsJSX={itemsJSX}
                      itemsKeys={itemsKeys}
                      rowIndex={rowIndex}
                    />
                  );
                })
              : _.times(loadingSize, (rowIndex) => (
                  <TableRow key={rowIndex.toString()}>
                    {_.times(columnNames.length, (columnIndex) => (
                      <TableCell key={columnIndex.toString()}>
                        <Skeleton animation="wave" />
                      </TableCell>
                    ))}
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </TableContainer>
      <ListNavigation
        actualPage={navigation?.actualPage}
        maxPage={navigation?.maxPage}
        updatePage={navigation?.updatePage}
      />
    </>
  );
}

export type ListNavigationProps = {
  actualPage: number | undefined;
  maxPage: number | undefined;
  updatePage: ((page: number) => void) | undefined;
};

/**
 * ListNavigation
 * @param {props} acutalPage
 * @param {props} maxPage
 * @param {props} updatePage
 * @constructor
 */
function ListNavigation({ actualPage, maxPage, updatePage }: ListNavigationProps) {
  const [displayedPage, setDisplayedPage] = useState<number>(actualPage ?? 1);

  useEffect(() => {
    if (actualPage) {
      setDisplayedPage(actualPage);
    }
  }, [actualPage]);

  if (actualPage === undefined || maxPage === undefined || !updatePage) {
    return null;
  }

  return (
    <NavigationLayout>
      <NavigationContainer>
        <IconButton
          onClick={() => {
            if (actualPage > 1) {
              updatePage(actualPage - 1);
            }
          }}>
          <ArrowBackIos />
        </IconButton>
        <TextField
          sx={{ width: '120px', margin: '7px 5px 7px 0' }}
          size={'small'}
          InputProps={{ inputProps: { min: 1, max: maxPage } }}
          type="number"
          value={prettierNumber(displayedPage.toString())}
          onChange={(e) => setDisplayedPage(Number(e.target.value))}
          onKeyDown={(e) => e.key === 'Enter' && updatePage(displayedPage)}
        />
        <Typography>{`/ ${prettierNumber(maxPage.toString())}`}</Typography>
        <IconButton
          onClick={() => {
            if (actualPage < maxPage) {
              window.scrollTo({ top: 0, behavior: 'smooth' });
              updatePage(actualPage + 1);
            }
          }}>
          <ArrowForwardIos />
        </IconButton>
      </NavigationContainer>
    </NavigationLayout>
  );
}

const NavigationLayout = styled('div')`
  display: flex;
  justify-content: flex-end;
  padding: 5px;
`;

const NavigationContainer = styled((props: PaperProps) => <Paper {...props} elevation={3} />)`
  display: flex;
  align-items: center;
`;

type RowType<T> = {
  columnNames: string[];
  itemsJSX: JSX.Element[];
  itemsKeys: string[];
  ShowMoreComponent?: React.FC<{ item: T; displayed: boolean }>;
  item: T;
  rowIndex: number;
};

/**
 * Row
 * @param {props} columnNames
 * @param {props} itemsJSX
 * @param {props} itemsKeys
 * @param {props} ShowMoreComponent
 * @param {props} item
 * @param {props} rowIndex
 * @constructor
 */
function Row<T>({
  columnNames,
  itemsJSX,
  itemsKeys,
  ShowMoreComponent,
  item,
  rowIndex
}: RowType<T>) {
  const [showMore, setShowMore] = useState(false);

  return (
    <>
      <TableRow>
        {Array.from(new Array(columnNames.length)).map((_, columnIndex) => {
          const id = itemsKeys[rowIndex] ?? rowIndex.toString();
          return (
            <TableCell
              key={id + columnIndex.toString()}
              sx={ShowMoreComponent ? { border: 'none' } : null}>
              {itemsJSX.length > columnIndex ? itemsJSX[columnIndex] : null}
            </TableCell>
          );
        })}
        {ShowMoreComponent && (
          <TableCell sx={{ border: 'none' }}>
            <IconButton
              component={Link}
              onClick={() => setShowMore((old) => !old)}
              sx={{
                transform: showMore ? 'rotate(180deg)' : 'rotate(0)',
                transition: 'all 0.2s linear'
              }}>
              <KeyboardArrowDownIcon />
            </IconButton>
          </TableCell>
        )}
      </TableRow>
      {ShowMoreComponent && (
        <TableRow>
          <MoreCellContainer colSpan={columnNames.length + 1}>
            <Collapse in={showMore} key={'more' + rowIndex.toString()}>
              <ShowMoreComponent item={item} displayed={showMore} />
            </Collapse>
          </MoreCellContainer>
        </TableRow>
      )}
    </>
  );
}

const MoreCellContainer = styled(TableCell)`
  padding: 0;
`;

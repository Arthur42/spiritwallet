import React from 'react';
import styled from '@mui/material/styles/styled';

/**
 * display footer
 * @constructor
 */
export default function Footer() {
  return <MainContainer />;
}

const MainContainer = styled('footer')({
  paddingTop: '30px'
});

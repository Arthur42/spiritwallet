import React, { useEffect } from 'react';
import TransactionType, { TransactionOperation } from '../../../types/transaction';
import Typography from '@mui/material/Typography';
import styled from '@mui/material/styles/styled';
import TokenValue from '../../text/TokenValue';
import { EGLD_ICON_URL, EGLD_IDENTIFIER, EGLD_NAME } from '../../../utils/config';
import ShrinkAddress from '../../text/address/ShrinkAddress';
import { walletLink } from '../../../utils/links';
import _ from 'lodash';
import Skeleton from '@mui/material/Skeleton';
import { useLazyApi } from '../../../hooks/useApi';
import TokenType from '../../../types/token';
import Paper, { PaperProps } from '@mui/material/Paper';

export type TransactionShowMoreProps = {
  transaction: TransactionType | undefined;
  walletAddress?: string;
  isLoading: boolean;
};

/**
 * ShowMore
 * @param {props} transaction
 * @param {props} walletAddress
 * @param {props} loading
 * @constructor
 */
export default function TransactionShowMore({
  transaction,
  walletAddress,
  isLoading
}: TransactionShowMoreProps) {
  return (
    <>
      <OperationsContainer>
        {isLoading ? (
          <OperationContainer>
            <Typography>
              <Skeleton animation="wave" width={500} />
            </Typography>
          </OperationContainer>
        ) : (
          <>
            {transaction && transaction.value !== '0' && (
              <ShowMoreValue
                value={transaction.value}
                from={transaction.sender}
                to={transaction.receiver}
              />
            )}
            {_.map(transaction?.operations, (operation) => (
              <ShowMoreOperation
                key={operation.id}
                operation={operation}
                walletAddress={walletAddress}
              />
            ))}
          </>
        )}
      </OperationsContainer>
    </>
  );
}

type ShowMoreOperationProps = {
  operation: TransactionOperation;
  walletAddress?: string;
};

/**
 * ShowMoreOperation
 * @param {props} operation
 * @param {props} walletAddress
 * @constructor
 */
function ShowMoreOperation({ operation, walletAddress }: ShowMoreOperationProps) {
  const [requestToken, token] = useLazyApi<TokenType>();

  useEffect(() => {
    if (operation.esdtType === 'FungibleESDT') {
      requestToken(`https://api.elrond.com/tokens/${operation.identifier}`);
    }
  }, []);

  return (
    <OperationContainer>
      {
        <>
          <Typography>{`${operation.type}\u00a0${operation.action}\u00a0:`}</Typography>
          <ShrinkAddress address={operation.sender} linkTo={walletLink.linkTo(operation.sender)} />
          <ShrinkAddress
            prefix={'to\u00a0:'}
            address={operation.receiver}
            linkTo={walletLink.linkTo(operation.receiver)}
          />
        </>
      }
      {operation.value && operation.identifier && (
        <OperationInformationContainer>
          <TokenValue
            name={operation.name ?? operation.collection ?? operation.identifier ?? EGLD_NAME}
            identifier={operation.identifier ?? EGLD_IDENTIFIER}
            value={operation.value}
            type={operation.type === 'nft' ? 'metaESDT' : 'ESDT'}
            decimals={operation.decimals}
            tokenImageUrl={
              operation.type === 'egld'
                ? EGLD_ICON_URL
                : token?.assets?.svgUrl ?? token?.assets?.pngUrl
            }
          />
        </OperationInformationContainer>
      )}
      {operation.data && (
        <OperationInformationContainer>
          <OperationDataContainer>
            <Typography>{operation.data}</Typography>
          </OperationDataContainer>
        </OperationInformationContainer>
      )}
    </OperationContainer>
  );
}

type ShowMoreValueProps = {
  value: string;
  from: string;
  to: string;
};

/**
 * ShowMoreOperation
 * @param {props} value
 * @param {props} from
 * @param {props} to
 * @constructor
 */
function ShowMoreValue({ value, from, to }: ShowMoreValueProps) {
  return (
    <OperationContainer>
      {
        <>
          <Typography>{`egld\u00a0transfer\u00a0:`}</Typography>
          <ShrinkAddress address={from} linkTo={walletLink.linkTo(from)} />
          <ShrinkAddress prefix={'to\u00a0:'} address={to} linkTo={walletLink.linkTo(to)} />
          <OperationInformationContainer>
            <TokenValue
              name={EGLD_NAME}
              identifier={EGLD_IDENTIFIER}
              value={value}
              tokenImageUrl={EGLD_ICON_URL}
            />
          </OperationInformationContainer>
        </>
      }
    </OperationContainer>
  );
}

const OperationsContainer = styled('div')`
  margin: 10px 0;
`;

const OperationContainer = styled('div')`
  display: flex;
  gap: 5px;
  margin: 4px 30px;
  padding: 2px 10px;
`;

const OperationInformationContainer = styled('div')`
  margin-left: 20px;
`;

const OperationDataContainer = styled((props: PaperProps) => <Paper elevation={3} {...props} />)`
  padding: 0 4px;
`;

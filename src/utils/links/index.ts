export const homeLink = {
  title: 'Spirit Explorer',
  linkTo: '/',
  routerUrl: '/'
};

export const walletLink = {
  title: (elrondWallet?: string) => `Wallet${elrondWallet && ` - ${elrondWallet}`}`,
  linkTo: (elrondWallet: string, section?: string) =>
    `/wallet/${elrondWallet}${section ? `/${section}` : ''}`,
  linkToOriginal: (elrondWallet: string) => `https://explorer.elrond.com/accounts/${elrondWallet}`,
  routerUrl: ['wallet/:elrondWallet', 'wallet/:elrondWallet/:section']
};

export const transactionLink = {
  title: (transactionIdentifier?: string) =>
    `Transaction${transactionIdentifier ? ` - ${transactionIdentifier}` : ''}`,
  linkTo: (transactionIdentifier: string) => `/transaction/${transactionIdentifier}`,
  linkToOriginal: (transactionIdentifier: string) =>
    `https://explorer.elrond.com/transactions/${transactionIdentifier}`,
  routerUrl: 'transaction/:transactionIdentifier'
};

export const tokenLink = {
  title: (tokenIdentifier?: string) => `Token${tokenIdentifier ? ` - ${tokenIdentifier}` : ''}`,
  linkTo: (tokenIdentifier: string) => `/token/${tokenIdentifier}`,
  routerUrl: 'token/:tokenIdentifier'
};

export const nftLink = {
  title: (nftIdentifier?: string) => `Nft${nftIdentifier ? ` - ${nftIdentifier}` : ''}`,
  linkTo: (nftIdentifier: string) => `/nft/${nftIdentifier}`,
  routerUrl: 'nft/:nftIdentifier'
};

export const nftCollectionLink = {
  title: (nftCollectionIdentifier?: string) =>
    `Collection${nftCollectionIdentifier ? ` - ${nftCollectionIdentifier}` : ''}`,
  linkTo: (nftCollectionIdentifier: string) => `/collection/${nftCollectionIdentifier}`,
  routerUrl: 'collection/:nftCollectionIdentifier'
};

export const explorerLink = {
  title: (section?: string) => `Explorer${section ? ` - ${section}` : ''}`,
  linkTo: (section?: string) => `/explorer${section ? `/${section}` : ''}`,
  routerUrl: ['explorer', 'explorer/:section']
};

export const delegatorLink = {
  title: (delegatorIdentity?: string) =>
    `Delegator${delegatorIdentity ? ` - ${delegatorIdentity}` : ''}`,
  linkTo: (delegatorIdentity: string) => `/delegator/${delegatorIdentity}`,
  routerUrl: 'delegator/:delegatorIdentity'
};

export const errorLink = {
  title: (error?: string) => `Error${error ? ` - ${error}` : ''}`,
  linkTo: (error?: string) => `/error${error ? `/${error}` : ''}`,
  routerUrl: ['*', '*/:error']
};

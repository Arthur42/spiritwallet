import _ from 'lodash';
import { NUMBER_COMMA } from './config';

/**
 * get readable quantity to display
 * @param {prop} quantity
 * @param {prop} fixed number after comma
 * @return {quantity}
 */
export default function prettierNumber(quantity: string, fixed?: number) {
  if (fixed) {
    quantity = parseFloat(quantity).toFixed(fixed).toString();
  }
  const values = _.split(quantity, NUMBER_COMMA);
  if (values[0]) {
    // put spacing every 3 number
    values[0] = values[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1\u00a0');
  }
  if (fixed && values[1] && values[1] === '0'.repeat(fixed)) {
    return values[0];
  }
  return _.join(values, '.');
}

/**
 * return transaction status color
 * @param {prop} status transaction status
 * @return {string} Chip color
 */
export default function getTransactionStatusColor(status: string) {
  switch (status) {
    case 'success':
      return 'success';
    case 'fail':
      return 'error';
    case 'invalid':
      return 'warning';
    default:
      return 'primary';
  }
}

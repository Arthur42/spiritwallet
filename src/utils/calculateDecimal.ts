import { NUMBER_COMMA } from './config';

export const DEFAULT_EGLD_DECIMAL = 18;

/**
 * calculateDecimal
 * @param {prop} value string value without comma
 * @param {prop} decimal number of numbers after the comma
 * @return {value} with comma
 */
export default function calculateDecimal(
  value: string,
  decimal: number = DEFAULT_EGLD_DECIMAL
): string {
  let res = '';

  // if value > 0
  if (value.length > decimal) {
    // add comma and value after and before comma
    res =
      value.slice(0, value.length - decimal) +
      NUMBER_COMMA +
      value.slice(value.length - decimal, value.length);
  } else {
    // add comma and value after comma
    res = '0' + NUMBER_COMMA + '0'.repeat(decimal - value.length) + value;
  }
  // remove useless zero
  if (res[res.length - 1] === '0') {
    res = res.split(/0*$/).join('');
  }
  // remove comma if nothing after
  if (res[res.length - 1] === NUMBER_COMMA) {
    res = res.substring(0, res.length - 1);
  }
  return res;
}

export const EGLD_NAME = 'EGLD';
export const EGLD_IDENTIFIER = 'WEGLD-bd4d79';
export const EGLD_ICON_URL = 'https://s2.coinmarketcap.com/static/img/coins/64x64/6892.png';

export const NUMBER_COMMA = '.';

export const DEFAULT_LIST_SIZE = 25;

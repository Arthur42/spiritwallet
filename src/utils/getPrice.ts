import prettierNumber from './prettierNumber';

/**
 * get readable price to display
 * @param {prop} quantity
 * @param {prop} unitPrice
 * @return {price}
 */
export default function getPrice(quantity: number, unitPrice: number): string {
  const price = (quantity * unitPrice).toFixed(2).toString() + '$';
  return prettierNumber(price);
}

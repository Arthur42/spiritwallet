import _ from 'lodash';

/**
 * toLowerAndSpaceCase
 * @param {prop} str
 * @return {string}
 */
export default function toLowerAndSpaceCase(str: string): string {
  str = _.join(_.split(str, /(?=[A-Z][a-z])/), ' ').toLowerCase();
  str = _.join(_.split(str, /_/), ' ').toLowerCase();
  return str;
}

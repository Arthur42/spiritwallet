import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Home from './routes/Home';
import Error from './routes/Error';
import Wallet from './routes/Wallet';
import Transaction from './routes/Transaction';
import {
  delegatorLink,
  errorLink,
  explorerLink,
  homeLink,
  nftCollectionLink,
  nftLink,
  tokenLink,
  transactionLink,
  walletLink
} from './utils/links';
import _ from 'lodash';
import Nft from './routes/Nft';
import NftCollection from './routes/NftCollection';
import Explorer from './routes/Explorer';
import Token from './routes/Token';
import Delegator from './routes/Delegator';

/**
 * Handle Routes with react-router-dom
 * @constructor
 */
export default function Router() {
  return (
    <Routes>
      <Route path={homeLink.routerUrl} element={<Home />} />
      {_.map(walletLink.routerUrl, (url) => (
        <Route path={url} key={url} element={<Wallet />} />
      ))}
      <Route path={transactionLink.routerUrl} element={<Transaction />} />
      <Route path={tokenLink.routerUrl} element={<Token />} />
      <Route path={nftLink.routerUrl} element={<Nft />} />
      <Route path={nftCollectionLink.routerUrl} element={<NftCollection />} />
      {_.map(explorerLink.routerUrl, (url) => (
        <Route path={url} key={url} element={<Explorer />} />
      ))}
      <Route path={delegatorLink.routerUrl} element={<Delegator />} />
      {_.map(errorLink.routerUrl, (url) => (
        <Route path={url} key={url} element={<Error />} />
      ))}
    </Routes>
  );
}

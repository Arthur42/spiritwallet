# Spirit Wallet

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## TODO

### Parameters

* Select date format
* local currency

### Token

* Change icon size
* Change egld image
* Change tokens display
* Give a total value in egld / local currency

### Transaction

* Lazy load, load 20 by 20 transactions
* add details
* Handle american date format (02 pm or 14h)
* Add details in explorer

### Nft

* Lazy load, load 20 by 20 nft
* Display more information
* Link to owner (or no owner)
* Link to creator
* Link to collection
* Show all nft or by collection

### Nft collection

* Show platforms only if it exists on it

### Delegation

* Work only in local (script can't execute on cors)

### Account

* Add accounts to explorer

### Smart contract

* to see

### List

* Manually set page number
* Change number of elements displayed
* search bar with search element selector (Hash / From / To ...)

### Other

* Handle theme in navigation, between pages
* Add unit test
* Add CI/CD unit test
* Add CI/CD code quality
* Config prettier with eslint
* Resolve path with webpack (@spirit/utils/...)
* SEO
* Select header search context (Transaction / Wallet ...)
* Make header responsive
* Make home page responsive
* Add page or pop-up with useful sites
* Change name (erdExplorer ?)
